<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Client;
use App\User;
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//->middleware('auth')
Route::prefix('admin')->middleware('auth')->group(function
() {
    Route::get('abonne', 'FacturesController@getAbonne')->name('abonne');
    Route::post('abonne', 'FacturesController@clientRegister')->name('abonne');
    Route::get('facture', 'FacturesController@getFactures')->name('facture');
    Route::post('facture', 'FacturesController@storeFacture')->name('facture');
    Route::post('facture/{id}', 'FacturesController@delete')->name('delete_facture');

    Route::get('/', function (){
        return view('admin.dashboard');})->name('dashboard');
    Route::get('client', function (){
//        $users = 'App\Client'::all( );
        return view('admin.client');
    })->name('client');
    Route::get('client/{id}', function ($id){
        $user = 'App\Client'::find($id);
        return view('admin.client_detail', compact('user'));
    })->name('client_detail');
    Route::post('client/update/{id}', 'ClientsController@update');
    Route::get('users', 'ManageUserController@getUser')->name('users');
    Route::get('user/profile/{id}', 'ManageUserController@getProfile')->name('profile_user');
    Route::get('user/edit/{id}', 'ManageUserController@getEdit')->name('edit_user');
    Route::put('user/edit/{id}', 'ManageUserController@editUser')->name('edit_user');
    Route::delete('users/{id}', 'ManageUserController@delete')->name('user.destroy');
    Route::get('getClient', function () {
        $users = 'App\Client'::all( );
        return $users;
    });
    Route::get('check_password/{id}/{password}', function ($id, $password) {
        $user = User::find($id);

//        dd(password_verify($password, $user->password));
        return (password_verify($password, $user->password)) ? 'true' : 'false';
    });
    Route::post('change_password/{id}', 'ManageUserController@password');
    Route::post('facture/edit/{id}', 'FacturesController@edit');
    Route::get('test', function () {
        return response()->file(asset('admin_asset/factures/1529632216.pdf'));
    });

    Route::prefix('cms')->group(function () {
        Route::get('home', 'CmsController@home')->name('home');
        Route::get('cabinet', 'CmsController@cabinet')->name('cabinet');
        Route::get('actualites', 'CmsController@actualites')->name('actualites');
        Route::get('contents', 'ContentsController@getContents');
        Route::get('contents/{id}', 'ContentsController@getContentsId');
        Route::post('content/edit/{id}', 'ContentsController@update');
        Route::post('content/image', 'ContentsController@addImage')->name('add_image_content');
        Route::post('/hero/store', 'heroController@store')->name('store_hero');
        Route::get('/hero/{id}', 'heroController@show');
        Route::get('heroes', 'heroController@index');
        Route::post('hero/edit/{id}', 'heroController@update');
        Route::get('services', 'servicesController@index')->name('services');
        Route::get('service/{id}', 'servicesController@show')->name('show_service');
        Route::post('service', 'servicesController@store')->name('store_service');
        Route::post('service/edit/{id}', 'servicesController@update')->name('edit_service');
        Route::post('service/image/{id}', 'servicesController@update_image_service')->name('update_image_service');

        Route::get('secteurs', 'secteursController@index')->name('secteurs');
        Route::get('secteur/{id}', 'secteursController@show')->name('show_secteur');
        Route::post('secteur', 'secteursController@store')->name('store_secteur');
        Route::post('secteur/edit/{id}', 'secteursController@update')->name('edit_secteur');
        Route::post('secteur/image/{id}', 'secteursController@update_image_secteur')->name('update_image_secteur');

        Route::get('nav', 'cmsController@nav');
        Route::get('nav/{id}', 'cmsController@nav_show');
        Route::post('nav/{id}', 'cmsController@editNav')->name('edit_nav');

        Route::get('histoire', 'cmsController@histoire')->name('edit_nav');
        Route::post('histoire', 'cmsController@store_histoire')->name('store_histoire');

        Route::post('membre/image', 'cmsController@edit_image_membre')->name('edit_image_membre');

        Route::post('membre', 'cmsController@store_membre')->name('store_membre');
        Route::post('membre/{id}', 'cmsController@update_membre')->name('update_member');

        Route::get('values', 'ContentsController@getValues');
        Route::get('value/{id}', 'ContentsController@getValue');
        Route::post('value/edit/{id}', 'ContentsController@editValue'); 
        
        Route::get('aspects', 'ContentsController@getAspects');
        Route::get('aspect/{id}', 'ContentsController@getAspect');
        Route::post('aspect/edit/{id}', 'ContentsController@editAspect');

        Route::get('infos', 'ContentsController@infos');
        Route::post('infos/edit', 'ContentsController@editInfos');

        Route::post('category/add', 'ActualiteController@addCategory')->name('add_category');
        Route::post('article/add', 'ActualiteController@addArticle')->name('add_article');
        Route::get('actualite/{id}', 'ActualiteController@showArticle')->name('show_article');


        Route::get('contact', 'cmsController@contact')->name('contact');
        Route::post('contact/image', 'cmsController@edit_image_contact')->name('edit_image_contact');

        Route::post('testamonials/', 'cmsController@create_testamonials')->name('store_testamonials');
        Route::post('aspect/', 'cmsController@create_aspect')->name('store_aspect');



    });
});

Route::get('test', 'test@show');
Route::post('test/{id}', 'test@process');
Route::get('email','ManageUserController@email')->name("email");
Route::get('/ajax/check_email', 'ManageUserController@checkmail');
Route::get('/ajax/check_facture', 'FacturesController@checkfacture');
Route::get('/search', 'FacturesController@search');
Route::get('admin/getFactures/{id}', function ($id) {
    $client = Client::find($id);
//    dd($client->factures);
//    return $client->factures->all();
    return response()->json($client->factures);
});
Route::get('admin/login', 'Auth\LoginController@showLoginForm')->name
('admin.login');