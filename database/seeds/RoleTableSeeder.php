<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'name' => 'Administrateur',
            'rank' => '1',
        ]);
        DB::table('roles')->insert([
            'name' => 'Comptabilite',
            'rank' => '2',
        ]);
        DB::table('roles')->insert([
            'name' => 'Web Manager',
            'rank' => '3',
        ]);
    }
}
