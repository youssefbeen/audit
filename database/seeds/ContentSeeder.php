<?php

use Illuminate\Database\Seeder;

class ContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //home page
        DB::table('contents')->insert([
            'id_perso' => '1',
            'title1' => "d'experts",
            'title2' => "Choisir une equipe",
            'description' => 'Notre équipe de conseillers est là pour vous servir.',
            'button' => 'EN SAVOIR PLUS',
            'media' => 'vide',
            'block' => 'hero',
            'page' => 'home'
        ]);
        DB::table('contents')->insert([
            'id_perso' => '2',
            'title1' => "Pourquoi Nous Choisir",
            'block' => 'raisons',
            'page' => 'home'
        ]);
        DB::table('contents')->insert([
            'id_perso' => '3',
            'title1' => "Reactivite",
            'title2' => "Lorem",
            'description' => 'Loreum Ipsum Doreum',
            'media' => 'vide',
            'block' => 'raisons',
            'page' => 'home'
        ]);
        DB::table('contents')->insert([
            'id_perso' => '4',
            'title1' => "Reactivite",
            'title2' => "Lorem",
            'description' => 'Loreum Ipsum Doreum',
            'media' => 'vide',
            'block' => 'raisons',
            'page' => 'home'
        ]);
        DB::table('contents')->insert([
            'id_perso' => '5',
            'title1' => "Reactivite",
            'title2' => "Lorem",
            'description' => 'Loreum Ipsum Doreum',
            'media' => 'vide',
            'block' => 'raisons',
            'page' => 'home'
        ]);
        DB::table('contents')->insert([
            'id_perso' => '6',
            'title1' => "Reactivite",
            'title2' => "Lorem",
            'description' => 'Loreum Ipsum Doreum',
            'media' => 'vide',
            'block' => 'raisons',
            'page' => 'home'
        ]);
        DB::table('contents')->insert([
            'id_perso' => '7',
            'title1' => "Nos Services",
            'title2' => "À La Hauteur De Vos Attentes.",
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.',
            'button' => 'Decouvrir',
            'block' => 'services',
            'page' => 'home'
        ]);
        DB::table('contents')->insert([
            'id_perso' => '8',
            'title1' => "A Propos De Notre Compagnie",
            'title2' => "Qui Sommes Nous",
            'description' => 'FIZAZI & ASSOCIÉS est une société à responsabilité limitée créée en 1999.',
            'button' => 'Decouvrir Plus',
            'block' => 'qui',
            'page' => 'home'
        ]);
        DB::table('contents')->insert([
            'id_perso' => '9',
            'title1' => "Intérêt du client 2000 projets.",
            'media' => 'vide',
            'block' => 'qui',
            'page' => 'home'
        ]);
        DB::table('contents')->insert([
            'id_perso' => '10',
            'title1' => "Écoute active prêt à vous aider.",
            'media' => 'vide',
            'block' => 'qui',
            'page' => 'home'
        ]);
        DB::table('contents')->insert([
            'id_perso' => '11',
            'title1' => "Professionnalisme satisfait de nous.",
            'media' => 'vide',
            'block' => 'qui',
            'page' => 'home'
        ]);
        DB::table('contents')->insert([
            'id_perso' => '12',
            'title1' => "Respect des délais meilleur service.",
            'media' => 'vide',
            'block' => 'qui',
            'page' => 'home'
        ]);
        DB::table('contents')->insert([
            'id_perso' => '13',
            'title1' => "Industries Couvertes",
            'title2' => "Dans Divers Secteurs D’activité",
            'block' => 'secteurs',
            'page' => 'home'
        ]);
        DB::table('contents')->insert([
            'id_perso' => '14',
            'title1' => "Dernières Nouvelles",
            'title2' => "Lorem Ipsum Dolor Set Amet",
            'block' => 'nouvelles',
            'page' => 'home'
        ]);
        DB::table('contents')->insert([
            'id_perso' => '15',
            'title1' => "Lorem ipsum dolor set amet consecteteur",
            'button' => "Contacter nos experts",
            'block' => 'contact',
            'page' => 'home'
        ]);


        //CABINET [20 - ...]

        DB::table('contents')->insert([
            'id_perso' => '20',
            'title1' => "À PROPOS DE NOUS",
            'description' => "FIZAZI & ASSOCIÉS est une société à responsabilité limitée créée en 1999. Elle est spécialisée en audit et le conseil et représente un acteur incontournable en matière fiscal sur le plan national.",
            'media' => "vide",
            'block' => 'hero',
            'page' => 'cabinet'
        ]);
        DB::table('contents')->insert([
            'id_perso' => '21',
            'title1' => "A Propos De Notre Compagnie",
            'title2' => "qui sommes nous",
            'block' => 'qui',
            'page' => 'cabinet'
        ]);
        DB::table('contents')->insert([
            'id_perso' => '22',
            'title1' => "Notre Histoire En Mots",
            'title2' => "Lorem Ipsum Dolor Setss",
            'block' => 'histoire',
            'page' => 'cabinet'
        ]);
        DB::table('contents')->insert([
            'id_perso' => '23',
            'title1' => "Nos valeurs",
            'title2' => "Meilleur De Nous",
            'description' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Aenean euismod bibendum laoreet",
            'block' => 'valeurs',
            'page' => 'cabinet'
        ]);
        DB::table('contents')->insert([
            'id_perso' => '24',
            'title1' => "Rencontrez Notre Équipe",
            'title2' => "Derrière Notre Succès",
            'description' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Aenean euismod bibendum laoreet",
            'block' => 'valeurs',
            'page' => 'cabinet'
        ]);

        //SERVICES [30 - ...] / SECTEURS / Activites


        DB::table('contents')->insert([
            'id_perso' => '30',
            'title1' => "SERVICES",
            'description' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.",
            'media' => "vide",
            'block' => 'hero',
            'page' => 'services'
        ]);
        DB::table('contents')->insert([
            'id_perso' => '31',
            'title1' => "SECTEURS",
            'description' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.",
            'media' => "vide",
            'block' => 'hero',
            'page' => 'secteurs'
        ]);
        DB::table('contents')->insert([
            'id_perso' => '32',
            'title1' => "ACTUALITES",
            'description' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.",
            'media' => "vide",
            'block' => 'hero',
            'page' => 'actualites'
        ]);
        DB::table('contents')->insert([
            'id_perso' => '33',
            'title1' => "Contactez-Nous",
            'description' => "N'hésitez pas à nous demander quelque chose, notre équipe toujours prête à vous aider.",
            'block' => 'hero',
            'page' => 'contact'
        ]);
        DB::table('contents')->insert([
            'id_perso' => '34',
            'title1' => "Explorez Notre Service",
            'title2' => "Lorem ipsum",
            'description' => "Nos principaux services dans le cadre de l’expertise comptable couvrent les aspects suivants",
            'block' => 'content',
            'page' => 'service'
        ]);
        DB::table('contents')->insert([
            'id_perso' => '35',
            'title1' => "Explorez Notre Secteur",
            'title2' => "Lorem Ipsum",
            'description' => "Nos principaux services dans le cadre de l’expertise comptable couvrent les aspects suivants",
            'block' => 'content',
            'page' => 'service'
        ]);

        //nav

        DB::table('navs')->insert([
            'position' => '1',
            'name' => "acceuil",
        ]);
        DB::table('navs')->insert([
            'position' => '2',
            'name' => "cabinet",
        ]);
        DB::table('navs')->insert([
            'position' => '3',
            'name' => "services",
        ]);
        DB::table('navs')->insert([
            'position' => '4',
            'name' => "secteurs",
        ]);
        DB::table('navs')->insert([
            'position' => '5',
            'name' => "actualite",
        ]);
        DB::table('navs')->insert([
            'position' => '6',
            'name' => "contact",
        ]);
        DB::table('navs')->insert([
            'position' => '7',
            'name' => "espace pro",
        ]);



        //info

        DB::table('infos')->insert([
            'adresse' => 'Lot Y5, Secteur 14, Avenue Addolb, Hay Ryad, Rabat',
            'tel' => "+212 5 37 71 63 98 - 99",
            'fax' => "+212 5 37 71 38 00",
        ]);

        //

        //VALUES

        DB::table('values')->insert([
            'id_perso' => '1',
            'name' => "Professionnalisme",
            'description' => "Nos collaborateurs sont emplis de professionnalisme dans leur gestion des différentes missions qu’ils sont...",
        ]);
        DB::table('values')->insert([
            'id_perso' => '2',
            'name' => "Intérêt du client",
            'description' => "Dans les missions de conseil, d’assistance, de supervision ou d’accompagnement que nous...",
        ]);
        DB::table('values')->insert([
            'id_perso' => '3',
            'name' => "Respect des délais",
            'description' => "S’il est une chose à laquelle nos collaborateurs prêtent particulièrement attention en premier...",
        ]);
        DB::table('values')->insert([
            'id_perso' => '4',
            'name' => "Proximité",
            'description' => "Etre au plus près du client, de leurs problèmes, de leurs préoccupations ou de leurs soucis de gestion...",
        ]);
        DB::table('values')->insert([
            'id_perso' => '5',
            'name' => "Écoute active",
            'description' => "Comme on dit souvent dans cette affirmation populaire « la solution se trouve parfois dans le problème »...",
        ]);
        DB::table('values')->insert([
            'id_perso' => '6',
            'name' => "Réactivité",
            'description' => "« Le temps c’est de l’or » dit-on.C’est la raison pour laquelle, fort de notre effectif, d’une trentaine de...",
        ]);



        DB::table('users')->insert([

            'email' => "admin@admin.com",
            'password' => bcrypt('admin'),
            'name' => 'Admin',
            'role_id' => '1',

        ]);

    }
}
