<?php

use Illuminate\Database\Seeder;

class CurrencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('currencies')->insert([
            'name' => 'MAD',
        ]);
        DB::table('roles')->insert([
            'name' => 'EUR',
        ]);
        DB::table('roles')->insert([
            'name' => 'USD',
        ]);
    }
}
