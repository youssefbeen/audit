<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablesCmsDivers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('heroes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title1')->nullable();
            $table->string('title2')->nullable();
            $table->string('button')->nullable();
            $table->text('description')->nullable();
            $table->string('media')->nullable();
            $table->timestamps();
        });

            Schema::create('aspects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description');
            $table->string('image')->nullable();
            $table->boolean('visibility')->default(1);
            $table->integer('service_id')->nullable();
            $table->integer('secteur_id')->nullable();
            $table->timestamps();
        });
        Schema::create('services', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->string('image')->nullable();
            $table->timestamps();
        });
            Schema::create('secteurs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->string('image')->nullable();
            $table->timestamps();
        });
            Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });
        Schema::create('actualites', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description');
            $table->string('image')->nullable();
            $table->integer('user_id')->nullable();
            $table->timestamps();
        });
        Schema::create('infos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('adresse')->nullable();
            $table->string('tel')->nullable();
            $table->string('fax')->nullable();
            $table->string('facebook')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('twitter')->nullable();
            $table->string('instagram')->nullable();
            $table->string('image')->nullable();

            $table->timestamps();
        });
        Schema::create('position', function (Blueprint $table) {
            $table->increments('id');
            $table->string('x');
            $table->string('y');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actualites');
        Schema::dropIfExists('heroes');
        Schema::dropIfExists('services');
        Schema::dropIfExists('infos');
        Schema::dropIfExists('position');
        Schema::dropIfExists('categories');
        Schema::dropIfExists('secteurs');
        Schema::dropIfExists('aspects');
    }
}
