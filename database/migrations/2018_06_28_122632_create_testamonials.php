<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestamonials extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('testamonials', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('entreprise')->nullable();
            $table->text('description');
            $table->string('image')->nullable();
            $table->integer('service_id')->nullable();
            $table->integer('secteur_id')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('testamonials');
    }
}
