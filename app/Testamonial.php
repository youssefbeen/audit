<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Testamonial extends Model
{
    protected $fillable = ['name', 'entreprise', 'image', 'service_id', 'secteur_id', 'description'];
}
