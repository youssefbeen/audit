<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aspect extends Model
{
    protected $fillable = ['name', 'description', 'image', 'service_id', 'secteur_id'];
}
