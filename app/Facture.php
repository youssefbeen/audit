<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facture extends Model
{
    protected $primaryKey = 'n_facture';

    protected $fillable = ['n_facture', 'montant', 'note', 'user_id', 'currency_id', 'client_id' ];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }
}
