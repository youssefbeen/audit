<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hero extends Model
{
    protected $fillable = ['title1', 'title2', 'description', 'media', 'button'];
}
