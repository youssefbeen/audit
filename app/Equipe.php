<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipe extends Model
{
    protected $fillable = ['name', 'position', 'email', 'image', 'linkedin', 'facebook', 'twitter'];
}
