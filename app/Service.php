<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = ['name', 'description', 'image'];

    public function testamonials() {
        return $this->hasMany('App\Testamonial');
    }

    public function aspects() {
        return $this->hasMany('App\Aspect');
    }
}
