<?php

namespace App\Http\Controllers;
use App\Content;
use App\Value;
use App\Aspect;
use App\Info;
use Illuminate\Http\Request;

class ContentsController extends Controller
{
    public function getContents() {
        return json_encode(Content::all());
    }

    public function getContentsId($id) {

        $content = Content::where('id_perso', '=', $id)->get();
        return json_encode($content);
    }

    public function update(Request $request, $id) {

        $hero = Content::where('id_perso', '=', $id)->first();
        $hero->title1 = $request->title1;
        $hero->title2 = $request->title2;
        $hero->description = $request->description;
        $hero->button = $request->button;

        $hero->save();
    }

    public function addImage(Request $request) {

        $id = $request->id;
        $content = Content::where('id_perso', '=', $id)->first();

        if ($request->hasFile('file')) {
            $media = $request->file('file');
            $filename = time(). '.' . $media->getClientOriginalExtension();
            $location = public_path('cms_asset/assets/media');
            $media->move($location, $filename);
            $oldfilename = $content->media;
            $content->media = $filename;
//            if ($oldfilename !== '') {
//                Storage::disk('facture')->delete($oldfilename);
//            }
        }

        $content->save();
    }



    public function getValues() {
        return json_encode(Value::all());
    }

    public function getValue($id) {

        $value = Value::where('id_perso', '=', $id)->first();
        return json_encode($value);
    }

    public function editValue(Request $request, $id) {

        $value = Value::where('id_perso', '=', $id)->first();
        $value->name = $request->name;
        $value->description = $request->description;
        $request->visible ? $value->visibility = $request->visible : $value->visibility = 0;


        $value->save();
    } 
    
    public function getAspects() {
        return json_encode(Aspect::all());
    }

    public function getAspect($id) {

        $value = Aspect::find($id);
        return json_encode($value);
    }

    public function editAspect(Request $request, $id) {

        $value = Aspect::find($id);
        $value->name = $request->name;
        $value->description = $request->description;
        $request->visible ? $value->visibility = $request->visible : $value->visibility = 0;


        $value->save();
    }


    public function infos() {
        return json_encode(Info::first());
    }



    public function editInfos(Request $request) {

        $value = Info::first();
        $value->tel = $request->tel;
        $value->fax = $request->fax;
        $value->adresse = $request->adresse;



        $value->save();
    }
}
