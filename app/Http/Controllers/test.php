<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class test extends Controller
{
    public function show()
    {
        return view('test');
    }
    public function process(Request $request)
    {
        return $request->title;
    }

}
