<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Storage;
use App\Facture;
use App\Client;
use App\Mail\NewUserWelcom;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
class FacturesController extends Controller
{



    public function clientRegister(Request $request) {
//        dd($request);
        $request['password'] = str_random(8); //generate password
        $user = Client::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
            'isClient' => $request['isClient'],
        ]);

        Mail::to($user->email)->send(new NewUserWelcom($request['password']));

        activity()->useLog('Client Ajoute')
            ->log('Ajout nouveau Client: '. $user->name);
        session()->flash('status', 'success');
        session()->flash('message', 'User created successfully!');

    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'n_facture' => 'required|integer|min:4|max:7|unique',
            'montant' => 'required|integer',
            'note' => 'string',
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
        ]);
    }

    public function getFactures()
    {
        $currencies = 'App\Currency'::get();
        $factures = Facture::orderBy('created_at', 'DESC')->get();
        return view('factures.index', compact('currencies', 'factures'));
    }
    /**
     * @param Request $request
     */
    public function storeFacture(Request $request)
    {
        $data = $request->all();
        $this->validator($request->all());
        $user = Client::where('email', '=', $request['email'])->exists();
        $request['isClient'] = 1;
        if(!$user) { $this->clientRegister($request); }

        $user = Client::where('email', '=', $data['email'])->first();
        $user->isClient = 1;
        $user->save();
            $facture = Facture::create([
                'n_facture' => $data['n_facture'],
                'montant' => $data['montant'],
                'note' => $data['note'],
                'client_id' => $user->id,
                'currency_id' => $data['currency_id']
            ]);

        activity()->useLog('Facture Cree')
            ->log('Creation Facture N: '.$facture->n_facture. 'du Client:' .$user->name);
            return redirect()->to(route('facture'));
    }

    public function checkfacture(Request $request) {
        return json_encode(!Facture::where('n_facture', '=',$request['n_facture'])->exists());
    }

    public function search(Request $request)
    {
        $result[]= "";
        $users = Client::where('name', 'like', '%'.$request['term']
            .'%')->get();

        foreach($users as $u){
            $result[] = ['label' => $u->name,
                'id' => $u->id
            ];
        }
        return $result;
    }

    public function delete($id) {
        $facture = Facture::where('n_facture', '=', $id)->first();
//        dd($facture);
        $num = $facture->n_facture;
        $facture->delete();
        activity()->useLog('Facture Supprimee')
            ->log('Suppression Facture Numero: '. $num);
        session()->flash('status', 'success');
        session()->flash('message', 'Facture Supprimee avec Succes!');

        return redirect()->to(route('facture'));

    }

    public function getAbonne() {
        return view('admin.newsletter');
    }

    public function edit (Request $request, $id) {
//        dd($request);
        
        $facture = Facture::where('n_facture', '=', $id)->first();
        if ($request->hasFile('file')){
            $fact = $request->file('file');
            $filename = time(). '.' . $fact->getClientOriginalExtension();
            $location = public_path('admin_asset/factures/');
            $fact->move($location, $filename);
            $oldfilename = $facture->facture;
            $facture->facture = $filename;
            if ($oldfilename !== '') {
                Storage::disk('facture')->delete($oldfilename);
            }

        }

        $facture->save();

        activity()->useLog('Facture Jointe')
            ->log('Jointe Facture N: '. $facture->n_facture);


        $request->session()->flash('message', 'Facture Jointe avec succes');
        $request->session()->flash('status', 'success');

        return redirect()->to(route('facture'));
    }



}
