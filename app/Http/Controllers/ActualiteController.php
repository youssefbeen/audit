<?php

namespace App\Http\Controllers;

use App\Actualite;
use App\Category;
use Illuminate\Http\Request;

class ActualiteController extends Controller
{
    public function addCategory (Request $request) {
        Category::create([
            'name' => $request->name,
        ]);

        return back();
    }

    public function addArticle (Request $request) {

        if ($request->hasFile('file')) {
            $media = $request->file('file');
            $filename = time(). '.' . $media->getClientOriginalExtension();
            $location = public_path('cms_asset/assets/media');
            $media->move($location, $filename);
        }
        $actualite = Actualite::create([
            'title' => $request->title,
            'description' => $request->description,
            'user_id' => $request->user_id,
            'image' =>$filename
        ]);


        $actualite->categories()->attach($request->category_id);


        return back();
    }

    public function showArticle($id) {
        $article = Actualite::find($id);
        return view('cms.actualite', compact('article'));
    }


}
