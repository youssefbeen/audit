<?php

namespace App\Http\Controllers;

use App\Hero;
use App\Histoire;
use App\Info;
use App\Nav;
use App\Equipe;
use App\Secteur;
use App\Actualite;
use App\Category;
use App\Aspect;
use App\Service;
use App\Testamonial;
use Illuminate\Http\Request;

class CmsController extends Controller
{
    public function home() {
        $heroes = Hero::all();
        $secteurs = Secteur::all();
        $services = Service::all();
        return view('cms.home', compact('heroes', 'secteurs', 'services'));
    }
    public function cabinet() {
        $histoires = Histoire::orderBy('date', 'DESC')->get();
        $membres = Equipe::all();
        $secteurs = Secteur::all();
        return view('cms.cabinet', compact('histoires', 'membres', 'secteurs'));
    }

    public function actualites() {
        $actualites = Actualite::all();
        $categories = Category::all();
        return view('cms.actualites', compact('actualites', 'categories'));
    }

    public function nav() {

        return json_encode(Nav::all());
    }
    public function editNav(Request $request, $id) {
        $nav = Nav::where('position', '=', $id)->first();
        $nav->name = $request->name;
        $nav->save();
    }
    public function nav_show($id) {

        $nav = Nav::where('position', '=', $id)->first();
        return json_encode($nav);
    }

    public function store_histoire (Request $request) {

        if ($request->hasFile('file')) {
            $media = $request->file('file');
            $filename = time(). '.' . $media->getClientOriginalExtension();
            $location = public_path('cms_asset/assets/media');
            $media->move($location, $filename);
            $request['image'] = $filename;
        }
        Histoire::create($request->all());

        return back();

    }

    public function store_membre (Request $request) {

        if ($request->hasFile('file')) {
            $media = $request->file('file');
            $filename = time(). '.' . $media->getClientOriginalExtension();
            $location = public_path('cms_asset/assets/media');
            $media->move($location, $filename);
            $request['image'] = $filename;
        }
        Equipe::create($request->all());

        return back();

    }
    public function update_membre (Request $request, $id) {

        $membre = Equipe::find($id);
        $membre->name = $request->name;
        $membre->position = $request->position;
        $membre->email = $request->email;
        $membre->facebook = $request->facebook;
        $membre->linkedin = $request->linkedin;
        $membre->twitter = $request->twitter;

        $membre->save();

        return back();

    }

    public function edit_image_membre (Request $request)
    {
//        dd($request);
        $id = $request->id;
        $membre = Equipe::find($id);
        if ($request->hasFile('file')) {
            $media = $request->file('file');
            $filename = time(). '.' . $media->getClientOriginalExtension();
            $location = public_path('cms_asset/assets/media');
            $media->move($location, $filename);
            $membre->image = $filename;
        }

        $membre->save();

        return back();
    }

    public function contact () {
        return view('cms.contact');
    }

    public function edit_image_contact (Request $request)
    {
        $membre = Info::first();
        if ($request->hasFile('file')) {
            $media = $request->file('file');
            $filename = time(). '.' . $media->getClientOriginalExtension();
            $location = public_path('cms_asset/assets/media');
            $media->move($location, $filename);
            $membre->image = $filename;
        }

        $membre->save();

        return back();
    }


    public function create_testamonials (Request $request) {

        if ($request->hasFile('file')) {
            $media = $request->file('file');
            $filename = time(). '.' . $media->getClientOriginalExtension();
            $location = public_path('cms_asset/assets/media');
            $media->move($location, $filename);
            $request['image'] = $filename;
        }
        Testamonial::create($request->all());

        return back();

    }

    public function create_aspect (Request $request) {

        if ($request->hasFile('file')) {
            $media = $request->file('file');
            $filename = time(). '.' . $media->getClientOriginalExtension();
            $location = public_path('cms_asset/assets/media');
            $media->move($location, $filename);
            $request['image'] = $filename;
        }
        Aspect::create($request->all());

        return back();

    }
}

