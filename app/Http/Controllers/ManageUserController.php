<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewUserWelcom;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use function MongoDB\BSON\toJSON;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;
use Spatie\Activitylog\Models\Activity;


class ManageUserController extends Controller
{
    public function editUser(Request $request, $id)
    {
        $user = User::findOrFail($id);


        $data= $request;
        $data['role_id'] = $user->role_id;
//        dd($request);
        $this->validate($data, [
            'name' => 'required',
//            'email' => 'required|email',
            'role_id' => 'required'
        ]);

        $user->name = $data['name'];
        $user->role_id = $data['role_id'];

//        dd($request);
        if ($request->hasFile('featured_image')){
            $image = $request->file('featured_image');
            $filename = time(). '.' . $image->getClientOriginalExtension();
            $location = public_path('admin_asset/images/'.$filename);

            Image::make($image)->resize(72,72)->save($location);
            $oldfilename = $user->image;
            $user->image = $filename;

            if ($oldfilename !== 'default.jpg') {
                Storage::delete($oldfilename);
            }

        }

//        dd($user);
        $user->save();

        activity()->useLog('Utilisateur Modifie')
            ->log('Modification utilisateur: '. $user->name);


        $request->session()->flash('message', 'User updated successfully.');
        $request->session()->flash('status', 'success');

        return redirect()->to(route('users'));
    }

    public function delete($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        activity()->useLog('Utilisateur Supprime')
            ->log('Suppression utilisateur: '. $user->name);
//        session('success', 'deleted');
        return redirect()->to(route('users'))->with('status', 'this message');
    }
    public function email()
    {
        Mail::to(Auth::user()->email)->send(new NewUserWelcom());
        return redirect()->to('home');
    }

    public function checkmail(Request $request)
    {
//        return($request['email']);
        return json_encode(!User::where('email', '=', $request['email'])->exists());
    }



    public function getUser() {
        $roles = Role::get();
        $users = User::get();
        return view('admin.users', compact('users', 'roles'));

    }
    public function getEdit($id) {
        $u = User::find($id);
        return view('admin.edit_user', compact('u'));

    }

    public function getProfile($id) {
        $user = User::find($id);


        $act = Activity::orderBy('created_at', 'DESC')->get();

//        dd($act->first());
//        dd($user);
//        dd($act->first()->causer->id);
        return view('admin.user_profile', compact('user', 'act'));
    }

    public function password (Request $request, $id) {
        dd($request->password);
        $request->validate([
            'password'=> 'min:6|required',
        ]);
        $user = User::find($id);
        $user->password = bcrypt($request->password);
        return 'success';
}
}
