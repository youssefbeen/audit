<?php

namespace App\Http\Controllers;

use App\Hero;
use Illuminate\Http\Request;

class HeroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return json_encode(Hero::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $filename = null;

        if ($request->hasFile('file')) {
            $media = $request->file('file');
            $filename = time(). '.' . $media->getClientOriginalExtension();
            $location = public_path('cms_asset/assets/media');
            $media->move($location, $filename);
        }
//        dd($request);
        Hero::create([
            'title1' => $request->title1,
            'title2' => $request->title2,
            'description' => $request->description,
            'media' => $filename
        ]);


        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $hero = Hero::find($id);
        return json_encode($hero);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
//        $hero = Hero::find($id);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $hero = Hero::find($id);
        $hero->title1 = $request->title1;
        $hero->title2 = $request->title2;
        $hero->description = $request->description;
        $hero->button = $request->button;

        if ($request->hasFile('file')) {
            $media = $request->file('file');
            $filename = time(). '.' . $media->getClientOriginalExtension();
            $location = public_path('cms_asset/assets/media');
            $media->move($location, $filename);
            $hero->media = $filename;
        }

        $hero->save();

//        return json_encode('success');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
