<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\Request;

class servicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::all();
        return view('cms.services', compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request);
        if ($request->hasFile('file')) {
            $media = $request->file('file');
            $filename = time(). '.' . $media->getClientOriginalExtension();
            $location = public_path('cms_asset/assets/media');
            $media->move($location, $filename);
            $request['image'] = $filename;
        }
        Service::create($request->all());

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $service = Service::find($id);
        return view('cms.service', compact('service'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $service = Service::find($id);
        $service->name = $request->name;
        $service->description = $request->description;

        $service->save();
        return back();
    }

    public function update_image_service(Request $request, $id)
    {
        $service = Service::find($id);
        if ($request->hasFile('file')) {
            $media = $request->file('file');
            $filename = time(). '.' . $media->getClientOriginalExtension();
            $location = public_path('cms_asset/assets/media');
            $media->move($location, $filename);
            $service->image = $filename;
        }

        $service->save();
        return back();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
