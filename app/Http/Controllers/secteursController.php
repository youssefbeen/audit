<?php

namespace App\Http\Controllers;

use App\Secteur;
use Illuminate\Http\Request;

class secteursController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $secteurs = Secteur::all();
        return view('cms.secteurs', compact('secteurs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request);
        if ($request->hasFile('file')) {
            $media = $request->file('file');
            $filename = time(). '.' . $media->getClientOriginalExtension();
            $location = public_path('cms_asset/assets/media');
            $media->move($location, $filename);
            $request['image'] = $filename;
        }
        Secteur::create($request->all());

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $secteur = Secteur::find($id);
        return view('cms.secteur', compact('secteur'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $secteur = secteur::find($id);
        $secteur->name = $request->name;
        $secteur->description = $request->description;

        $secteur->save();
        return back();
    }
    public function update_image_secteur(Request $request, $id)
    {
        $secteur = secteur::find($id);
        if ($request->hasFile('file')) {
            $media = $request->file('file');
            $filename = time(). '.' . $media->getClientOriginalExtension();
            $location = public_path('cms_asset/assets/media');
            $media->move($location, $filename);
            $secteur->image = $filename;
        }

        $secteur->save();
        return back();
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
