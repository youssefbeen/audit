<?php

namespace App\Http\Controllers;
use App\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use function PHPSTORM_META\type;


class ClientsController extends Controller
{
    public function update(Request $request, $id) {
//        dd($request);
            $client = Client::find($id);
//            dd($request->tel);
            $client->name = $request->name;
//        dd(!$request->tel !== 'null');
            if ($request->tel !== 'null') {
//                dd($request->tel);
                $client->tel = $request->tel;
            }

        if ($request->hasFile('image')){
            $image = $request->file('image');
            $filename = time(). '.' . $image->getClientOriginalExtension();
            $location = public_path('admin_asset/images/'.$filename);

            Image::make($image)->resize(72,72)->save($location);
            $oldfilename = $client->image;

            $client->image = $filename;
            if ($oldfilename !== 'default.jpg') {
                Storage::delete($oldfilename);
            }

        }
        $client -> save();
//        dd($client);

        activity()->useLog('Client Modifie')
            ->log('Modification Client: '. $client->name);

        $request->session()->flash('message', 'Client updated successfully.');
        $request->session()->flash('status', 'success');
            return response()->json(['success' => 'suceeees']);
    }
}
