$(document).ready(function() {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var id;
    var contents = [];
    var hero = [];
    var content;
    var nav = [];
    var values = [];
    var aspects = [];
    var i;


    //INFOS

    function getInfos() {
        $.ajax({
            type: 'GET',
            url: '/admin/cms/infos',
            dataType: 'json',
            success: function (data) {
                i = data;
                loadInfos();
            },
            error: function () {
                alert("error get")
            }
        })
    }

    getInfos();

    function loadInfos () {
        $('.tel').text(i.tel);
        $('.adresse').text(i.adresse);
        $('.fax').text(i.fax);
        $('.image').attr('src', '/cms_asset/assets/media/' + i.image);
        // $('.mail').text(i.email);
    }



    $('#editInfos').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        id = button.data('id') // Extract info from data-* attributes
        var modal = $(this);

        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        $.ajax({
            type: 'GET',
            url: '/admin/cms/infos',
            dataType: 'json',
            success: function (data) {
                // alert(data[0].title1)
                modal.find('#tel').val(data.tel);
                modal.find('#fax').val(data.fax);
                modal.find('#adresse').val(data.adresse);
                modal.find('form').submit(function (event) {
                    event.preventDefault();
                    $.ajax({
                        type: 'post',
                        data: $(this).serialize(),
                        url: '/admin/cms/infos/edit',
                        success: function (data) {
                            getInfos();
                            modal.modal('hide');
                        }
                    })
                });
            },
            error: function () {
                alert("error get after edit")
            }
        })


    })

    //
    
    //ASPECTS
    
    function getAspects() {
        $.ajax({
            type: 'GET',
            url: '/admin/cms/aspects',
            dataType: 'json',
            success: function (data) {
                aspects = data.slice(0)
                loadAspects();
            },
            error: function () {
                alert("error")
            }
        })
    }

    getAspects();

    function loadAspects () {
        for (v of aspects) {
            if (v.visibility)  {
                $('#aspect_' + v.id + ' .name').text(v.name);
                $('#aspect_' + v.id + ' .description').text(v.description);
                $('#aspect_' + v.id).parent().css('opacity', '1');

            }
            else {
                $('#aspect_' + v.id_perso).parent().css('opacity', '0.3');
            }

        }
    }


    $('#editAspect').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        id = button.data('id') // Extract info from data-* attributes
        var modal = $(this);

        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        $.ajax({
            type: 'GET',
            url: '/admin/cms/aspect/' + id,
            dataType: 'json',
            success: function (data) {
                // alert(data[0].title1)
                modal.find('#name').val(data.name);
                modal.find('#description').val(data.description);
                if (data.visibility) { modal.find('#visible').attr('checked', 'checked'); }
                modal.find('form').submit(function (event) {
                    event.preventDefault();
                    $.ajax({
                        type: 'post',
                        data: $(this).serialize(),
                        url: '/admin/cms/aspect/edit/' + id,
                        success: function (data) {
                            getAspects();
                            modal.modal('hide');
                        }
                    })
                });
            },
            error: function () {
                alert("error")
            }
        })


    })

    //
    
    
    
    //

    //VALUES

    function getValues() {
        $.ajax({
            type: 'GET',
            url: '/admin/cms/values',
            dataType: 'json',
            success: function (data) {
                values = data.slice(0)
                loadValues();
            },
            error: function () {
                alert("error")
            }
        })
    }

    getValues();

    function loadValues () {
        for (v of values) {
            if (v.visibility)  {
                $('#value_' + v.id_perso + ' .name').text(v.name);
                $('#value_' + v.id_perso + ' .description').text(v.description);
                $('#value_' + v.id_perso).parent().css('opacity', '1');

            }
            else {
                $('#value_' + v.id_perso).parent().css('opacity', '0.3');
            }

        }
    }


    $('#editValue').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        id = button.data('id') // Extract info from data-* attributes
        var modal = $(this);

        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        $.ajax({
            type: 'GET',
            url: '/admin/cms/value/' + id,
            dataType: 'json',
            success: function (data) {
                // alert(data[0].title1)
                modal.find('#name').val(data.name);
                modal.find('#description').val(data.description);
                if (data.visibility) { modal.find('#visible').attr('checked', 'checked'); }
                modal.find('form').submit(function (event) {
                    event.preventDefault();
                    $.ajax({
                        type: 'post',
                        data: $(this).serialize(),
                        url: '/admin/cms/value/edit/' + id,
                        success: function (data) {
                            getValues();
                            modal.modal('hide');
                        }
                    })
                });
            },
            error: function () {
                alert("error")
            }
        })


    })

    //


    function getContent() {
        $.ajax({
            type: 'GET',
            url: '/admin/cms/contents',
            dataType: 'json',
            success: function (data) {
                contents = data.slice(0)
                load();
            },
            error: function () {
                alert("error")
            }
        })
    }
     function getContentById(id) {
            $.ajax({
                type: 'GET',
                url: '/admin/cms/contents/id',
                dataType: 'json',
                success: function (data) {
                    content = data.slice(0)
                    load();
                },
                error: function () {
                    alert("error")
                }
            })
        }

    getContent();

    function load () {
        for (c of contents) {
            $('#' + c.id_perso + ' .title1').text(c.title1);
            $('#' + c.id_perso + ' .title2').text(c.title2);
            $('#' + c.id_perso + ' .description').text(c.description);
            $('#' + c.id_perso + ' .boutton').text(c.button);
            $('#' + c.id_perso).parents('.back').css('background-image', 'url(/cms_asset/assets/media/' + c.media + ')');
            // $('#' + c.id_perso).parents('.back').css('background-color', 'red');
        }
    }

    //NAAAAV

    function getNav() {
        $.ajax({
            type: 'GET',
            url: '/admin/cms/nav',
            dataType: 'json',
            success: function (data) {
                // alert(data);
                nav = data.slice(0)
                loadNav();
            },
            error: function () {
                alert("error NAV")
            }
        })
    }

    getNav();

    function loadNav () {
        for (c of nav) {
            $('#nav_' + c.position).text(c.name);
            // $('#' + c.id_perso).parents('.back').css('background-color', 'red');
        }
    }

    $('#editNav').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var id = button.data('id');

        var modal = $(this);

        $.ajax({
            type: 'GET',
            url: '/admin/cms/nav/' + id,
            dataType: 'json',
            success: function (data) {
                modal.find('#name').val(data.name);
                modal.find('form').submit(function (event) {
                    event.preventDefault();
                    $.ajax({
                        type: 'post',
                        data: $(this).serialize(),
                        url: '/admin/cms/nav/' + id,
                        success: function (data) {
                            getNav();
                            modal.modal('hide');
                        }
                    })
                });
            }
        });
    });



    ///



    $(".editContent").on("hidden.bs.modal", function() {
        id = null;
        var modal = $(this);
        modal.find('#titre1').show();
        modal.find('#titre2').show();
        modal.find('#description_dev').show();
        modal.find('#boutton_div').show();
    });
    $('#editContent').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        id = button.data('id') // Extract info from data-* attributes
        var modal = $(this);

        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        $.ajax({
            type: 'GET',
            url: '/admin/cms/contents/' + id,
            dataType: 'json',
            success: function (data) {
                // alert(data[0].title1)
                // modal.find('#title1').val(data[0].title1);
                // modal.find('#title1').val(data[0].title1);
                data[0].title1 == null ? modal.find('#titre1').hide() : modal.find('#title1').val(data[0].title1) ;
                data[0].title2 == null ? modal.find('#titre2').hide() : modal.find('#title2').val(data[0].title2);
                data[0].description == null ? modal.find('#description_div').hide() : modal.find('#description').val(data[0].description);
                data[0].button == null ? modal.find('#boutton_div').hide() : modal.find('#boutton').val(data[0].button);
                modal.find('form').submit(function (event) {
                    event.preventDefault();
                    $.ajax({
                        type: 'post',
                        data: $(this).serialize(),
                        url: '/admin/cms/content/edit/' + id,
                        success: function (data) {
                            getContent();
                            modal.modal('hide');
                        }
                    })
                });
                // data[0].title2 == '' ? modal.find('input #title2').hide() : modal.find('input #title2').val(data[0].title2);
                // modal.find('.modal-title').text(content[0].title)
                // modal.find('.modal-body input').val(recipient)
                // modal.find('#sub').click(function () {
                //     load()
                // })
            },
            error: function () {
                alert("error")
            }
        })


    })

    function getHero() {
        $.ajax({
            type: 'GET',
            url: '/admin/cms/heroes',
            dataType: 'json',
            success: function (data) {
                hero = data.slice(0);
                loadHero();
            },
            error: function () {
                alert("error get hero")
            }
        })
    }
    getHero();
    function loadHero() {
        for (h of hero) {
            h.title1 == null ? $('#hero_' + h.id + ' .title1').hide() : $('#hero_' + h.id + ' .title1').text(h.title1);
            h.title2 == null ? $('#hero_' + h.id + ' .title2').hide() : $('#hero_' + h.id + ' .title2').text(h.title2);
            h.description == null ? $('#hero_' + h.id + ' .description').hide() : $('#hero_' + h.id + ' .description').text(h.description);
            h.button == null ? $('#hero_' + h.id + ' .boutton').hide() : $('#hero_' + h.id + ' .boutton').text(h.button);
            // $('#imgi').attr('src', '/cms_asset/assets/images/home/slide1.jpg');
            $('#hero_' + h.id).parents('.back').css('background-image', 'url(/cms_asset/assets/media/' + h.media + ')');

        }
    }





$('#modalHero').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var id = button.data('id');
        var edit = button.data('edit');

    // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this);

    })

$('#editHero').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var id = button.data('id');
        var edit = button.data('edit');

    // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this);

            $.ajax({
                type: 'GET',
                url: '/admin/cms/hero/' + id,
                dataType: 'json',
                success: function (data) {
                    // alert(data.title1)
                    // alert(data[0].title1)

                    modal.find('form').attr('action', '/admin/cms/hero/edit/' + id);
                    // $('#form_edit').attr('action', 'skhdfj');
                    modal.find('input[name="title1"]').val(data.title1);
                    modal.find('input[name="title2"]').val(data.title2);
                    modal.find('input[name="description"]').val(data.description);
                    modal.find('form').submit(function (event) {
                        event.preventDefault();
                        $.ajax({
                            type: 'post',
                            // data: {'test': $("#test").val(),
                            //         'name': "youssef",
                            //         '_token': CSRF_TOKEN
                            // },
                            data: $(this).serialize(),
                            url: '/admin/cms/hero/edit/' + id,
                            success: function (data) {
                                getHero();
                                modal.modal('hide');
                            }
                        })
                    });
                },
                error: function () {
                    alert("error edit")
                }
            })




    })

    $('#editMembre').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var id = button.data('id');
        var name = button.data('name');
        var email = button.data('email');
        var position = button.data('position');
        var twitter = button.data('twitter');
        var facebook = button.data('facebook');
        var linkedin = button.data('linkedin');

        // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this);
        modal.find('#name').val(name);
        modal.find('#position').val(position);
        modal.find('#email').val(email);
        modal.find('#facebook').val(facebook);
        modal.find('#linkedin').val(linkedin);
        modal.find('#twitter').val(twitter);
        modal.find('form').submit(function (event) {
            event.preventDefault();
            $.ajax({
                type: 'post',
                data: $(this).serialize(),
                url: '/admin/cms/membre/' + id,
                success: function (data) {
                    location.reload();
                    modal.modal('hide');
                },
                error: function () {
                    alert("error edit membre")
                }
            });

        })

    });

    $('#editImage_Membre').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        id = button.data('id');
        var modal = $(this);
        modal.find('.id').val(id);
    });



    //DROPZONEJS


    Dropzone.options.createAspect = {
        paramName: "file", // The name that will be used to transfer the file
        maxFilesize: 2,
        clickable: '#createAspect .upl-box',
        addRemoveLinks: true,

        autoProcessQueue: false,
        init: function() {
            var myDrop = this;

            // First change the button to actually tell Dropzone to process the queue.
            this.element.querySelector("button[type=submit]").addEventListener("click", function (e) {
                // Make sure that the form isn't actually being sent.
                e.preventDefault();
                e.stopPropagation();
                myDrop.processQueue();
            });
            this.on("addedfile", function (file) {
                $('#createAspect .upl-box').hide();
            });
            this.on("removedfile", function (file) {
                $('#createAspect .upl-box').show();
            });
            this.on("success", function (file) {
                location.reload();
                $('#createAspect').modal('hide');
            });

        }

    };
    Dropzone.options.createTestamonials = {
        paramName: "file", // The name that will be used to transfer the file
        // maxFilesize: 2,
        // autoProcessQueue: false,
        addRemoveLinks: true,
        clickable: '.upl-box',
        autoProcessQueue: false,
        init: function() {
            var myDrop = this;

            // First change the button to actually tell Dropzone to process the queue.
            this.element.querySelector("button[type=submit]").addEventListener("click", function (e) {
                // Make sure that the form isn't actually being sent.
                e.preventDefault();
                e.stopPropagation();
                myDrop.processQueue();
            });
            this.on("addedfile", function (file) {
                $('.upl-box').hide();
            });
            this.on("removedfile", function (file) {
                $('.upl-box').show();
            });
            this.on("success", function (file) {
                location.reload();
                $('#createTestamonials').modal('hide');
            });

        }

    };

    Dropzone.options.editImageContact = {
        paramName: "file", // The name that will be used to transfer the file
        // autoProcessQueue: false,
        addRemoveLinks: true,
        init: function() {
            this.on("success", function (file) {
                location.reload();
            });

        }

    };

    Dropzone.options.createArticle = {
        paramName: "file", // The name that will be used to transfer the file
        // maxFilesize: 2,
        // autoProcessQueue: false,
        addRemoveLinks: true,
        clickable: '.upl-box',
        autoProcessQueue: false,
        init: function() {
            var myDrop = this;

            // First change the button to actually tell Dropzone to process the queue.
            this.element.querySelector("button[type=submit]").addEventListener("click", function (e) {
                // Make sure that the form isn't actually being sent.
                e.preventDefault();
                e.stopPropagation();
                myDrop.processQueue();
            });
            this.on("addedfile", function (file) {
                $('.upl-box').hide();
            });
            this.on("removedfile", function (file) {
                $('.upl-box').show();
            });
            this.on("success", function (file) {
                location.reload();
                $('#createMembre').modal('hide');
            });

        }

    };
    Dropzone.options.editImageMembre = {
        paramName: "file", // The name that will be used to transfer the file
        // autoProcessQueue: false,
        addRemoveLinks: true,
        init: function() {
            this.on("success", function (file) {
                location.reload();
            });

        }

        };
    Dropzone.options.editImageService = {
        paramName: "file", // The name that will be used to transfer the file
        // autoProcessQueue: false,
        addRemoveLinks: true,
        init: function() {
            this.on("success", function (file) {
                location.reload();
            });

        }

        };
    Dropzone.options.editImageSecteur = {
        paramName: "file", // The name that will be used to transfer the file
        // autoProcessQueue: false,
        addRemoveLinks: true,
        init: function() {
            this.on("success", function (file) {
                location.reload();
            });

        }

        };
    Dropzone.options.createMembre = {
        paramName: "file", // The name that will be used to transfer the file
        // maxFilesize: 2,
        // autoProcessQueue: false,
        addRemoveLinks: true,
        clickable: '.upl-box',
        autoProcessQueue: false,
        init: function() {
            var myDrop = this;

            // First change the button to actually tell Dropzone to process the queue.
            this.element.querySelector("button[type=submit]").addEventListener("click", function (e) {
                // Make sure that the form isn't actually being sent.
                e.preventDefault();
                e.stopPropagation();
                myDrop.processQueue();
            });
            this.on("addedfile", function (file) {
                $('.upl-box').hide();
            });
            this.on("removedfile", function (file) {
                $('.upl-box').show();
            });
            this.on("success", function (file) {
                location.reload();
                $('#createMembre').modal('hide');
            });

        }

        };
 Dropzone.options.createHistoire = {
        paramName: "file", // The name that will be used to transfer the file
        // maxFilesize: 2,
        // autoProcessQueue: false,
        addRemoveLinks: true,
        clickable: '.upl-box',
        autoProcessQueue: false,
        init: function() {
            var myDrop = this;

            // First change the button to actually tell Dropzone to process the queue.
            this.element.querySelector("button[type=submit]").addEventListener("click", function (e) {
                // Make sure that the form isn't actually being sent.
                e.preventDefault();
                e.stopPropagation();
                myDrop.processQueue();
            });
            this.on("addedfile", function (file) {
                $('.upl-box').hide();
            });
            this.on("removedfile", function (file) {
                $('.upl-box').show();
            });
            this.on("success", function (file) {
                location.reload();
                $('#createHistoire').modal('hide');
            });

        }

        };

    Dropzone.options.createSecteur = {
        paramName: "file", // The name that will be used to transfer the file
        // maxFilesize: 2,
        // autoProcessQueue: false,
        addRemoveLinks: true,
        clickable: '.upl-box',
        autoProcessQueue: false,
        init: function() {
            var myDrop = this;

            // First change the button to actually tell Dropzone to process the queue.
            this.element.querySelector("button[type=submit]").addEventListener("click", function (e) {
                // Make sure that the form isn't actually being sent.
                e.preventDefault();
                e.stopPropagation();
                myDrop.processQueue();
            });
            this.on("addedfile", function (file) {
                $('.upl-box').hide();
            });
            this.on("removedfile", function (file) {
                $('.upl-box').show();
            });
            this.on("success", function (file) {
                location.reload();
                $('#createSecteur').modal('hide');
            });

        }

        };
    Dropzone.options.createService = {
        paramName: "file", // The name that will be used to transfer the file
        // maxFilesize: 2,
        // autoProcessQueue: false,
        addRemoveLinks: true,
        clickable: '.upl-box',
        autoProcessQueue: false,
        init: function() {
            var myDrop = this;

            // First change the button to actually tell Dropzone to process the queue.
            this.element.querySelector("button[type=submit]").addEventListener("click", function (e) {
                // Make sure that the form isn't actually being sent.
                e.preventDefault();
                e.stopPropagation();
                myDrop.processQueue();
            });
            this.on("addedfile", function (file) {
                $('.upl-box').hide();
            });
            this.on("removedfile", function (file) {
                $('.upl-box').show();
            });
            this.on("success", function (file) {
                $('#createService').modal('hide');
            });

        }

        };
Dropzone.options.formEdit = {
        paramName: "file", // The name that will be used to transfer the file
        // maxFilesize: 2,
        // autoProcessQueue: false,
        addRemoveLinks: true,
        clickable: '.upl-box',
        autoProcessQueue: false,
        init: function() {
            var myDropzone = this;

            // First change the button to actually tell Dropzone to process the queue.
            this.element.querySelector("button[type=submit]").addEventListener("click", function (e) {
                // Make sure that the form isn't actually being sent.
                e.preventDefault();
                e.stopPropagation();
                myDropzone.processQueue();
            });
            this.on("addedfile", function (file) {
                $('.upl-box').hide();
            });
            this.on("removedfile", function (file) {
                $('.upl-box').show();
            });
            this.on("success", function (file) {
                $('#form-edit').modal('hide');
            });

        }

        };


    Dropzone.options.myDropzone = {
        init: function () {
            this.on("processing", function (file) {
                this.options.url = "/admin/cms/content/image/";
            });
            // this.on("success"), function (file) {
            //     console.log('success');
            // };
        },
        paramName: "file", // The name that will be used to transfer the file
        // maxFilesize: 2,
        // autoProcessQueue: false,
        addRemoveLinks: true,
        success: function () {
            $('#editImage').modal('hide');
            getContent();
        }// MB
        // accept: function(file, done) {
        //     if (file.name == "justinbieber.jpg") {
        //         done("Naha, you don't.");
        //     }
        //     else { done(); }
        // }
    };

    $('#editImage').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        id = button.data('id') // Extract info from data-* attributes
        // alert(id)
        var modal = $(this);
        modal.find('.id').val(id);

    });

    // $('#editImage').on('show.bs.modal', function (event) {
    //     var button = $(event.relatedTarget) // Button that triggered the modal
    //     id = button.data('id') // Extract info from data-* attributes
    //     var modal = $(this);
    //
    //     $('div#drop').dropzone({
    //         url: "/admin/cms/content/image/" + id,
    //         headers: {
    //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //         },
    //         // acceptedFiles: 'application/pdf',
    //         addRemoveLinks: true,
    //
    //     });
    // });

    //on hide i will reload the content

});

// i will request for the content
//doing loop of content
//put data into the Dom
//modoification modal data (passing id)
//submitiing form
//on hide update data


//to do:

// make schema for content (make number specifi MANUAL increment)
// seed essential content
// link with the Dom with id
// make forms for update data

