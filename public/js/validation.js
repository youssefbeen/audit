$(document).ready(function(){



    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var id = -1;

    $('#editUser').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var name = button.data('name');
        var role = button.data('role');
        var email = button.data('email');
        var position = button.data('position');
        id = button.data('id');
        var modal = $(this);
        modal.find('.modal-title').text('Modifier ' + name);
        modal.find('#name').val(name)
        modal.find('#email').val(email)
        modal.find('#role').val(role)
        modal.find('#position').val(position)
        modal.find('form').attr('action', "http://localhost:8000/admin/users/" + id)
        // var r = $("#role option[value='"+role+"']")
        modal.find("#role_id option[value='"+role+"']").attr("selected","selected");
    });
    $('#joindre_facture').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var n_facture = button.data('id');
        var montant = button.data('montant');
        var email = button.data('email');
        var name = button.data('name');
        var status = button.data('status');
        var modal = $(this);
        modal.find('#numero').text(n_facture);
        modal.find('#client').text(name)
        modal.find('#mail').text(email)
        modal.find('#montant').text(montant)
        $('#joindre_facture_form').attr('action', "http://localhost:8000/admin/facture/edit/" + n_facture)
        if(status === '') {
            $('#pdf').hide()
            var myDropzone = new Dropzone("div#upl", {
                url: "/admin/facture/edit/" + n_facture,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                acceptedFiles: 'application/pdf',
                addRemoveLinks: true,
            });
        }
        else {
            $("#upl").hide();
            $('#link_pdf').attr('href', '/admin_asset/factures/'+status)
            $("#change").click(function () {
                $('#pdf').hide()
                $("#upl").show();
                var myDropzone = new Dropzone("div#upl", {
                    url: "/admin/facture/edit/" + n_facture,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    acceptedFiles: 'application/pdf',
                    addRemoveLinks: true,

                });

            })

        }
        // myDropzone.on("queuecomplete", function (file) {
        //     location.reload();
        // });


        // modal.find('form').attr('action', "http://localhost:8000/admin/users/" + id)
        // // var r = $("#role option[value='"+role+"']")
        // modal.find("#role_id option[value='"+role+"']").attr("selected","selected");
    });



    // $('#editClient').on('show.bs.modal', function (event) {
    //     var button = $(event.relatedTarget); // Button that triggered the modal
    //
    //     var name = button.data('name');
    //
    //     var tel = button.data('tel');
    //     var email = button.data('email');
    //     var factures = button.data('factures');
    //
    //     var status = button.data('status');
    //     id = button.data('id');
    //     var modal = $(this);
    //     modal.find('.modal-title').text('Modifier ' + name);
    //     modal.find('#name').val(name)
    //     modal.find('#email').val(email)
    //     // modal.find('#show').click(function () {
    //     //     $.ajax({
    //     //         type: 'GET',
    //     //         url: 'getFactures/'+id,
    //     //         dataType: 'json',
    //     //         success: function(data){
    //     //             alert(data[0].n_facture);
    //     //         },
    //     //         error:function () {
    //     //             alert("error")
    //     //         }
    //     //     })
    //     })
    //
    //     // var r = $("#role option[value='"+role+"']")
    //     // modal.find("#role_id option[value='"+role+"']").attr("selected","selected");
    // });


    $("#facture_form").validate({
        errorClass: "c-input--danger",
        successClass: "c-input--success",
        errorElement: "span",
        rules:{
            name: 'required',
            email: {
                required: true,
                email: true,
},
            n_facture: {
                required: true,
                digits:true,
                remote: "/ajax/check_facture"
            },
            montant: {
                required: true,
                digits:true
            },
            currency_id: {
                required: true,
            }

        },
        messages: {
            currency_id: "",
            n_facture:{
                remote: "N {0} already taken"
            }
        }
    });
    $("#addForm").validate({
        errorClass: "c-input--danger",
        successClass: "c-input--success",
        errorElement: "span",
        rules:{
            name: 'required',
            email: {
                required: true,
                email: true,
                remote: "/ajax/check_email"
            },
            role_id: {
                required: true,
            },


        },
        messages: {
            role_id: "",
            email:{
                remote: "{0} already taken"
            }
        }
    });
    $("#edit-user-form").validate({

        rules:{
            name: 'required',
            tel: {
                digits: true,
            },

        },


});

    $("#edit_client_form").validate({
        errorClass: "c-input--danger",
        successClass: "c-input--success",
        errorElement: "span",
        rules:{
            name: 'required',
            tel: {
                digits: true,
            },
            image: {
                extension: "jpg"
            }
        },
        // messages: {
        //     role_id: "",
        //
        // },


});



        // var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        // $("#editForm").submit(function (e) {
        //
        //     // if($("#editForm").valid()){
        //     //  e.preventDefault();
        //
        //     // $.ajax({
        //     //
        //     //     headers: {
        //     //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //     //     },
        //     //     type: "PUT",
        //     //     url: "users/" + id,
        //     //
        //     //     // data: $(this).serialize(),
        //     //     data: new FormData($(this)),
        //     //     dataType: 'json',
        //     //     // contentType: false,
        //     //     processData:false,
        //     //
        //     //     success: function (data) {
        //     //         location.reload();
        //     //
        //     //     },
        //     //     beforeSend: function (xhr) {
        //     //         xhr.setRequestHeader('X-CSRF-TOKEN', $("#token").attr('content'));
        //     //     },
        //     // })
        //     // }
        //
        // })
        // // var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');


    $("#formtest").submit(function () {
        $.ajax({
            type: 'post',
            // data: {'test': $("#test").val(),
            //         'name': "youssef",
            //         '_token': CSRF_TOKEN
            // },
            data: $(this).serialize(),
            url: 'test/'+ id,
            success:function(data) {
                alert(data)
            }
        })
    })

    $("#search").focus(function () {
        $(this).css('background-image', 'none')
        $(this).addClass("search_focus");
    });
    $("#search").blur(function () {
        if(!$(this).val()){
            $(this).css('background-image', "url(\'../images/search.svg\')")
            $(this).removeClass("search_focus")
        }

    });

    $(".flash").fadeOut(3000);

    // var source = [ { value: "www.foo.com",
    //     label: "Spencer Kline"
    // },
    //     { value: "www.example.com",
    //         label: "James Bond"
    //     },
    // ];


    $( "#search" ).autocomplete({
        source: '/search',
        select: function( event, ui ) {
            // var selector = '"#' + ui.item.value + '"';
            // alert(String(ui.item.id))
            $("#"+ ui.item.id).effect( "highlight", {color:"#669966"}, 1000 );
            // $("html, body").animate({ scrollTop: $('#'+ui.item.value).offset().top }, 1000);

        }
    });

    $('#editForm input[name="email"]').attr('disabled', true)

    //
    // $("#delete-form").submit(function (event) {
    //     event.preventDefault();
    //     // $("#btn-confirm").click(function () {
    //     //     $("#delete-form").submit();
    //     // })
    //     $('#modal-delete').modal({ backdrop: 'static', keyboard: false })
    //         .on('click', '#btn-confirm', function(){
    //             $("#delete-form").submit();
    //         });
    // })

    $('#modal-delete').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var id = button.data('id');
        $(this).find('form').attr('action', "http://localhost:8000/admin/users/" + id)
    });
    $('#modal-delete-facture').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var id = button.data('id');
        $(this).find('form').attr('action', "http://localhost:8000/admin/facture/" + id)
    })


});