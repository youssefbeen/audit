@extends('layouts.admin_sidebar')

@section('content')
    {{--<password :id="2"></password>--}}
    <div class="row">
        <div class="col-12">
            <nav class="c-tabs">
                <div class="c-tabs__list nav nav-tabs" id="myTab" role="tablist">

                    <a class="c-tabs__link active" id="nav-profile-tab" data-toggle="tab" href="#nav-profile"
                       role="tab" aria-controls="nav-profile" aria-selected="false">
                    <span class="c-tabs__link-icon">
                      <i class="feather icon-sliders"></i>
                    </span>Edit Profile
                    </a>
                </div>
                <div class="c-tabs__content tab-content" id="nav-tabContent">
                    <div class="c-tabs__pane active" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile">

                        <!-- content goes here -->
                        <form enctype="multipart/form-data" method="post" id="edit-user-form" action={{route('edit_user', $u->id)}} >
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                        <div class="row">
                            <div class="col-xl-4">
                                <div class="c-field u-mb-medium">
                                    <label class="c-field__label" for="name">Name</label>
                                    <input class="c-input" type="text" id="name" name="name" value="{{$u->name}}">
                                </div>

                                <div class="c-field u-mb-medium">
                                    <label class="c-field__label" for="email">Email Address</label>
                                    <input class="c-input" type="text" id="email" name="email" value="{{$u->email}}"
                                           disabled>
                                </div>


                            </div>

                            <div class="col-xl-4">

                                <div class="c-field u-mb-medium">
                                    <label class="c-field__label" for="position">Position</label>
                                    <input type="text" class="c-input" name="position" id="position">
                                </div>


                                <div class="c-field u-mb-medium">
                                    <label class="c-field__label" for="tel">Telephone</label>
                                    <input class="c-input" type="tel" id="tel" name="tel" value="{{$u->tel}}">
                                </div>


                            </div>

                            <div class="col-xl-4">

                                <div class="c-card u-text-center">
                                    <div class="c-avatar u-inline-flex">
                                        <img class="c-avatar__img" src={{asset('admin_asset/images/'.$u->image)}}>
                                    </div>

                                    <h5>{{$u->name}}</h5>
                                    <p class="u-pb-small u-mb-small u-border-bottom">{{$u->position}}</p>

                                    {{--<p class="u-h4">--}}
                                        {{--<a class="u-mr-xsmall" href="#">--}}
                                            {{--<i class="feather icon-facebook"></i>--}}
                                        {{--</a>--}}
                                        {{--<a class="u-mr-xsmall" href="#">--}}
                                            {{--<i class="feather icon-twitter"></i>--}}
                                        {{--</a>--}}
                                        {{--<a class="u-mr-xsmall" href="#">--}}
                                            {{--<i class="feather icon-github"></i>--}}
                                        {{--</a>--}}
                                    {{--</p>--}}
                                </div>

                            </div>

                            <div class="col-12 col-sm-7 col-xl-2 u-mr-auto u-mb-xsmall">
                                <button class="c-btn c-btn--info c-btn--fullwidth" type="submit" >Save
                                    Settings</button>
                            </div>
                        </div>
                        </form>

                        <span class="c-divider u-mv-medium"></span>

                        <div class="row">
                            {{--<div class="col-12 col-sm-7 col-xl-2 u-mr-auto u-mb-xsmall">--}}
                                {{--<button class="c-btn c-btn--info c-btn--fullwidth" type="submit" onclick="event--}}
                                {{--.preventDefault(); document.getElementById('edit-user-form').submit();">Save--}}
                                    {{--Settings</button>--}}
                            {{--</div>--}}

                            <div class="col-12 col-sm-5 col-xl-3 u-text-right">
                                <button class="c-btn c-btn--danger c-btn--fullwidth c-btn--outline"
                                        data-toggle="modal" data-target="#modal-delete" data-id="{{$u->id}}">Delete My
                                    Account</button>

                                <div class="c-modal c-modal--small modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modal-delete">
                                    <div class="c-modal__dialog modal-dialog" role="document">
                                        <div class="c-modal__content">
                                            <div class="c-modal__body">
                                    <span class="c-modal__close" data-dismiss="modal" aria-label="Close">
                                        <i class="feather icon-x"></i>
                                    </span>

                                                <span class="c-icon c-icon--danger c-icon--large u-mb-small">
                                      <i class="feather icon-alert-triangle"></i>
                                    </span>
                                                <h3 class="u-mb-small">Do you want to delete your account?</h3>

                                                <p class="u-mb-medium">By deleting you account, you no longer have access to your dashboard, members and your information.</p>

                                                <div class="u-text-center">
                                                    <form id="delete-form" method="POST">
                                                        {{ csrf_field() }}
                                                        <input name="_method" type="hidden" value="DELETE">


                                                        <a href="#" class="c-btn c-btn--danger c-btn--outline u-mr-small" data-dismiss="modal" aria-label="Close">Cancel</a>
                                                        <button class="c-btn c-btn--danger" id="btn-confirm" type="submit">Delete</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div><!-- // .c-modal__content -->
                                    </div><!-- // .c-modal__dialog -->
                                </div><!-- // .c-modal -->
                            </div>
                        </div>
                    </div>

                    <div class="c-tabs__pane" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">ducimus quam fuga, vero atque, error laboriosam odio provident eveniet nemo reiciendis non optio, laborum enim ipsum dolorum, voluptatem ducimus quam fuga, vero atque, error laboriosam odio provident eveniet nemo!</div>
                    <div class="c-tabs__pane" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">optio, laborum enim ipsum dolorum, voluptatem ducimus quam fuga, vero atque, error laboriosam odio provident eveniet nemo reicienoptio, laborum enim ipsum dolorum, voluptatem ducimus quam fuga, vero atque, error laboriosam odio provident eveniet nemo reicien</div>
                </div>
            </nav>
        </div>
    </div>


@stop