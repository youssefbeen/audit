@extends('layouts.admin_sidebar')

@section('content')

        <div class="row">
            <div class="col-md-7">

                <nav class="c-tabs">
                    <div class="c-tabs__list nav nav-tabs" id="myTab" role="tablist">
                        <a class="c-tabs__link active" id="nav-home-tab" data-toggle="tab"  role="tab"
                           aria-controls="nav-home" aria-selected="true">Factures</a>
                    </div>
                    <div class="c-tabs__content tab-content" id="nav-tabContent">
                        <div class="c-tabs__pane active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                            <div class="c-feed">
                                @foreach($user->factures as $f)
                                <div class="c-feed__item {{$f->statut ? 'c-feed__item--success' : ''}}">
                                    <div class="c-feed__item-content">
                                        <h6 class="u-text-small">Numero Facture: {{$f->n_facture}}</h6>
                                        <p>Montant: {{$f->montant}} {{$f->currency['name']}}</p>
                                        <p>Satut: {{$f->statut ? 'Paye' : 'En cours'}}</p>
                                    </div>
                                    <p class="u-text-xsmall">{{$f->created_at->diffForHumans()}}</p>
                                </div>
                                @endforeach

                                {{--<div class="c-feed__item c-feed__item--success">--}}
                                    {{--<div class="c-feed__item-content">--}}
                                        {{--<h6 class="u-text-small">Payment received</h6>--}}
                                        {{--<p>Cha-ching payment received from <a href="#">@client-22</a></p>--}}
                                    {{--</div>--}}
                                    {{--<p class="u-text-xsmall">30 mins ago</p>--}}
                                {{--</div>--}}


                            </div><!-- // .c-feed -->
                        </div>
                        <div class="c-tabs__pane" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">ducimus quam fuga, vero atque, error laboriosam odio provident eveniet nemo reiciendis non optio, laborum enim ipsum dolorum, voluptatem ducimus quam fuga, vero atque, error laboriosam odio provident eveniet nemo!</div>
                        <div class="c-tabs__pane" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">optio, laborum enim ipsum dolorum, voluptatem ducimus quam fuga, vero atque, error laboriosam odio provident eveniet nemo reicienoptio, laborum enim ipsum dolorum, voluptatem ducimus quam fuga, vero atque, error laboriosam odio provident eveniet nemo reicien</div>
                    </div>
                </nav>
            </div>

            <div class="col-md-5">
                <div class="c-card">
                    <div class="u-text-center">
                        <div class="c-avatar c-avatar--large u-mb-small u-inline-flex">
                            <img class="c-avatar__img" src={{asset('admin_asset/images/'.$user->image)}}
                                 alt="Avatar">
                        </div>

                        <h5>{{$user->name}}</h5>
                        <p>{{$user->position}}</p>
                    </div>

                    <span class="c-divider u-mv-small"></span>

                    <span class="c-text--subtitle">Email Address</span>
                    <p class="u-mb-small u-text-large">{{$user->email}}</p>

                    <span class="c-text--subtitle">Phone NUMBER</span>
                    <p class="u-mb-small u-text-large">{{$user->tel}}</p>

                </div>

            </div>
        </div>



@stop