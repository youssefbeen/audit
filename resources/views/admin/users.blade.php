@extends('layouts.admin_sidebar')

@section('content')

    <div class="container"  style="margin-bottom: 30px;">
        <button class="c-btn c-btn--info pull-right" data-toggle="modal" data-target="#addUser"><span class="glyphicon
        glyphicon-plus-sign"></span> Ajouter
            nouvel utilisateur</button>
        {{--<button class="btn btn-primary pull-right add" data-toggle="modal" data-target="#addUser"><span class="glyphicon glyphicon-plus-sign"  ></span>  Ajouter Utilisateur</button>--}}

    </div>

            <div class="row">
                @foreach($users as $key => $u)
                <div class="col-sm-6 col-xl-3">

                    <div class="c-card  u-text-center">
                        <div class="c-avatar u-inline-flex u-mb-small">
                            <img class="c-avatar__img" src="{{asset('admin_asset/images/'. $u->image)}}" alt="Clark">
                        </div>
                        <h5>{{$u->name}}</h5>
                        <p class="u-pb-small u-mb-small u-border-bottom">{{$u->role['name']}}</p>
                        @if($u->id !=2)
                        <p class="u-h4">
                            {{--<a class="u-mr-xsmall" style="float: right;" data-toggle="modal"--}}
                                                                                          {{--data-target="#editUser" data-id="{{$u->id}}" data-name="{{$u->name}}" data-email="{{$u->email}}"--}}
                                                                                          {{--data-position="{{$u->position}}"--}}
                                                                                          {{--data-role="{{$u->role['id']}}">--}}
                                {{--<i class="fas fa-edit"></i>--}}
                            {{--</a>--}}
                            <div class="c-dropdown dropdown" style="float: right;">
                             <span class="dropdown-toggle u-color-info" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                                <i class="feather icon-more-vertical"></i>
                              </span>
                                <div class="c-dropdown__menu dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="c-dropdown__item dropdown-item" href={{route('profile_user', $u->id)
                                    }}>View
                                        Profile</a>
                                    <a class="c-dropdown__item dropdown-item" href="{{route('edit_user', $u->id)
                                    }}">Edit
                                        Profile</a>

                                    <a class="c-dropdown__item dropdown-item c-btn c-btn--danger c-btn--outline" data-toggle="modal" data-target="#modal-delete"
                                       data-id = "{{$u->id}}" >Supprimer</a>
                                </div>
                            </div>
                        </p>
                        @endif
                        {{--<p class="u-h4">--}}
                            {{--<a class="u-mr-xsmall" href="#">--}}
                                {{--<i class="feather icon-facebook"></i>--}}
                            {{--</a>--}}
                            {{--<a class="u-mr-xsmall" href="#">--}}
                                {{--<i class="feather icon-twitter"></i>--}}
                            {{--</a>--}}
                            {{--<a class="u-mr-xsmall" href="#">--}}
                                {{--<i class="feather icon-github"></i>--}}
                            {{--</a>--}}
                        {{--</p>--}}
                    </div>

                </div>
                    @if($key==3)

                    </div>
                    <div class="row">
                        @endif
                   @if($key==7)

                    </div>
                     <div class="row">
                     @endif
                @endforeach


            </div>


    <div class="c-modal c-modal--small modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modal-delete">
        <div class="c-modal__dialog modal-dialog" role="document">
            <div class="c-modal__content">
                <div class="c-modal__body">
                                    <span class="c-modal__close" data-dismiss="modal" aria-label="Close">
                                        <i class="feather icon-x"></i>
                                    </span>

                    <span class="c-icon c-icon--danger c-icon--large u-mb-small">
                                      <i class="feather icon-alert-triangle"></i>
                                    </span>
                    <h3 class="u-mb-small">Do you want to delete your account?</h3>

                    <p class="u-mb-medium">By deleting you account, you no longer have access to your dashboard, members and your information.</p>

                    <div class="u-text-center">
                        <form id="delete-form" method="POST">
                            {{ csrf_field() }}
                            <input name="_method" type="hidden" value="DELETE">


                            <a href="#" class="c-btn c-btn--danger c-btn--outline u-mr-small" data-dismiss="modal" aria-label="Close">Cancel</a>
                        <button class="c-btn c-btn--danger" id="btn-confirm" type="submit">Delete</button>
                        </form>
                    </div>
                </div>
            </div><!-- // .c-modal__content -->
        </div><!-- // .c-modal__dialog -->
    </div><!-- // .c-modal -->
    {{--</form>--}}

    {{--<example-component></example-component>--}}

{{--<div class="container">--}}
    {{--<h1 class="pull-left">Ipsum</h1>--}}
    {{--<button class="btn btn-primary pull-right add" data-toggle="modal" data-target="#addUser"><span class="glyphicon glyphicon-plus-sign"  ></span>  Ajouter Utilisateur</button>--}}
{{--</div>--}}
{{--@if(session()->has('status'))--}}
    {{--<div class="alert alert-{{ session('status') }} flash">--}}
        {{--{!! session('message') !!}--}}
    {{--</div>--}}
{{--@endif--}}

{{--{{dd(session()->all())}}--}}

{{--<div class="container">--}}

    {{--<div class="row col-md-10 col-md-offset-1">--}}
        {{--<table class="table table-striped ">--}}

            {{--<tr>--}}
                {{--<th>Utilisateurs</th>--}}
                {{--<th>Position</th>--}}
                {{--<th>Email</th>--}}
                {{--<th></th>--}}
            {{--</tr>--}}

            {{--<tbody class="border-tab">--}}
            {{--@foreach($users as $u)--}}
            {{--<tr>--}}
                {{--<td>{{$u->name}}</td>--}}
                {{--<td>{{$u->role['name']}}</td>--}}
                {{--<td>{{$u->email}}</td>--}}
                {{--<td class="">--}}
                    {{--@if($u->id != 8)--}}
                    {{--<div class="dropdown">--}}
                        {{--<button class="btn  dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">--}}
                            {{--<span class="glyphicon glyphicon-option-vertical"></span>--}}
                        {{--</button>--}}
                        {{--<ul class="dropdown-menu pad"--}}
                            {{--aria-labelledby="dropdownMenu1">--}}
                            {{--<li><button class="btn btn-default action"  data-toggle="modal"--}}
                                        {{--data-target="#editUser" data-id="{{$u->id}}" data-name="{{$u->name}}" data-email="{{$u->email}}"--}}
                                {{--data-position="{{$u->position}}"--}}
                                        {{--data-role="{{$u->role['id']}}"> <i class="far fa-edit"></i>--}}
                                {{--Modifier</button></li>--}}

                            {{--<li>--}}
                                {{--<form action="{{ route('user.destroy', $u->id) }}" method="post">--}}
                                    {{--{{ csrf_field() }}--}}
                                    {{--{{ method_field('DELETE') }}--}}
                                    {{--<button type="submit"--}}
                                            {{--class="btn btn-danger--}}
                                            {{--action" onclick="return--}}
                                            {{--confirm('Are' + ' you sure?')"> <i class="fa fa-trash"></i> Delete</button>--}}
                                {{--</form>--}}
                            {{--</li>--}}

                        {{--</ul>--}}
                    {{--</div>--}}
                        {{--@endif--}}

                {{--</td>--}}
            {{--</tr>--}}
            {{--@endforeach--}}

            {{--</tbody>--}}
        {{--</table>--}}
    {{--</div>--}}
{{--</div>--}}

{{--<!-- line modal -->--}}
    <div class="c-modal modal fade" id="addUser" tabindex="-1" role="dialog" aria-labelledby="addUser">
        <div class="c-modal__dialog modal-dialog" role="document">
            <div class="c-modal__content">
                <div class="c-modal__body">
                <span class="c-modal__close" data-dismiss="modal" aria-label="Close">
                    <i class="feather icon-x"></i>
                </span>

                    <span class="c-icon c-icon--large u-mb-small">
                  <i class="feather icon-anchor"></i>
                </span>
                    <h3 class="u-mb-small">Ajouter nouvel utilisateur</h3>
                    <form action="{{route('register')}}" method="post" id="addForm">
                        {{ csrf_field() }}
                    @include('admin.partials.forms_user')

                    <div class="o-line" style="margin-top: 20px">
                        <a href="#" class="c-btn c-btn--info c-btn--outline">Previous</a>
                        <button class="c-btn c-btn--info" type="submit">Ajouter</button>
                    </div>
                    </form>
                </div>
            </div><!-- // .c-modal__content -->
        </div><!-- // .c-modal__dialog -->
    </div><!-- // .c-modal -->






{{--<div class="modal fade" id="addUser" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">--}}
    {{--<div class="modal-dialog">--}}
        {{--<div class="modal-content">--}}
            {{--<div class="modal-header">--}}
                {{--<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>--}}
                {{--<h3 class="modal-title text-center" id="lineModalLabel">Ajouter Utilisateur</h3>--}}
                {{--<p class="text-center">Ipsum lorum</p>--}}
            {{--</div>--}}
            {{--<div class="modal-body test">--}}

                {{--<!-- content goes here -->--}}
                {{--<form action="{{route('register')}}" method="post" id="addForm">--}}
                    {{--{{ csrf_field() }}--}}
                    {{--@include('admin.partials.forms_user')--}}
                    {{--<div class="col-md-offset-5">--}}
                        {{--<button type="submit" class="btn btn-primary">Ajouter</button>--}}
                    {{--</div>--}}
                {{--</form>--}}

            {{--</div>--}}

        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}

<div class="modal fade" id="editUser" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h3 class="modal-title text-center" id="lineModalLabel">Modifier Utilisateur</h3>
                <p class="text-center">Ipsum lorum</p>
            </div>
            <div class="modal-body test">

                <!-- content goes here -->
                <form id="editForm"  enctype="multipart/form-data" method="post">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    @include('admin.partials.forms_user')
                    <div class="form-group col-md-6">
                        <label for="featured_image">Image</label>
                        <input type="file" name="featured_image" id="featured_image">
                    </div>
                    <div class="col-md-offset-5 col-md-12">
                        <button type="submit" class="btn btn-primary">Modifier</button>
                    </div>
                </form>
                <div class="clearfix"></div>

            </div>

        </div>
    </div>
</div>


@stop