@extends('cms.app')

@section('content')

    <!--// [BEGIN] Contact Details //-->
    <section class="contact-details-area">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="contact-details-carousell">
                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#editImageContact">
                            Edit Image
                        </button>
                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#editInfos">
                            Edit Info
                        </button>
                        <div class="single-contact-details">

                            <div class="img-holder">
                                <img src="" class="image" alt="">
                            </div>
                            <div class="text-holder parallax-bg-one" style="background-image:url(assets/images/parallax-background/contact-details-bg.jpg);">
                                <div class="sec-title">
                                    <h1>Fizazi & Associés</h1>
                                    <div class="border left"></div>
                                </div>
                                <div class="inner-content">
                                    <ul>
                                        <li>
                                            <div class="icon-box">
                                                <span class="icon-signs"></span>
                                            </div>
                                            <div class="text-box">
                                                <p><span>Addresse : </span> </p> <p class="adresse"> Lot Y5, Secteur
                                                    14, Avenue Addolb, Hay Ryad, Rabat</p>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="icon-box">
                                                <span class="icon-technology"></span>
                                            </div>
                                            <div class="text-box">
                                                <p><span>Téléphone : </span> </p><p class="tel">+212 5 37 71 63 98 -
                                                    99</p>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="icon-box">
                                                <span class="icon-back"></span>
                                            </div>
                                            <div class="text-box">
                                                <p><span>Fax : </span> </p> <p class="fax"> +212 5 37 71 38 00</p>
                                            </div>
                                        </li>
                                    </ul>
                                    <ul class="sociallinks-style-one">
                                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--// [END] Contact Details //-->

    <!--// [BEGIN] Map Area //-->
    <section class="google-map-area">
        <div class="google-map-box">
            <div
                    class="google-map"
                    id="contact-google-map"
                    data-map-lat="33.9495883"
                    data-map-lng="-6.8758862"
                    data-icon-path="../assets/images/resources/map-marker.png"
                    data-map-title="Lot Y5, Secteur 14, Avenue Addolb, Hay Ryad, Rabat, Maroc"
                    data-map-zoom="12"
            >
            </div>
        </div>
    </section>
    <!--// [END] Map Area //-->

    <!--// [BEGIN] Contact Form  //-->
    <section class="contact-info-area">
        <div class="container">
            <button type="button" class="btn btn-primary" data-toggle="modal"
                    data-target="#editContent" data-id="33">
                Edit
            </button>
            <div class="row">
                <div class="col-md-12">
                    <div class="contact-form">
                        <div class="sec-title text-center" id="33">
                            <h1 class="title1">Contactez-nous</h1>
                            <div class="border center"></div>
                            <span class="description">N'hésitez pas à nous demander quelque chose, notre équipe
                                toujours prête
                                à vous
                                aider.</span>
                        </div>
                        <form id="contact-form" name="contact_form" class="default-form" action="" method="post">
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="text" name="form_name" value="" placeholder="Votre Nom" required="">
                                    <input type="email" name="form_email" value="" placeholder="Email Addresse" required="">
                                    <input type="text" name="form_subject" value="" placeholder="Sujet">
                                    <input type="text" name="form_phone" value="" placeholder="Téléphone">
                                </div>
                                <div class="col-md-6">
                                    <textarea name="form_message" placeholder="Votre Message..." required=""></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <input id="form_botcheck" name="form_botcheck" class="form-control" type="hidden" value="">
                                    <button class="btn-one" type="submit" data-loading-text="Please wait...">Envoyer</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--// [END] Contact Form  //-->

    <div class="modal fade" id="editImageContact" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h3 class="modal-title" id="lineModalLabel">My Modal</h3>
                </div>
                <div class="modal-body">
                    <form action="{{route('edit_image_contact')}}" method="post"
                          class="dropzone" id="edit-image-contact">
                        {{csrf_field()}}
                    </form>


                </div>

            </div>
        </div>
    </div>



@stop