@extends('cms.app')

@section('content')

    {{--<form action="/file-upload"--}}
          {{--class="dropzone"--}}
          {{--id="my-awesome-dropzone">--}}
    {{--</form>--}}


    <!--// [BEGIN] Page Hero //-->
    <section class="breadcrumb-area back">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <button type="button" class="btn btn-primary" data-toggle="modal"
                            data-target="#editContent" data-id="20">
                        Edit
                    </button>

                    <button type="button" class="btn btn-primary" data-toggle="modal"
                            data-target="#editImage" data-id="20">
                        Media
                    </button>

                    <div class="breadcrumbs" id="20">
                        <h1 class="title1">À PROPOS DE NOUS</h1>
                        <p class="description">
                            FIZAZI & ASSOCIÉS est une société à responsabilité limitée créée en 1999.
                            Elle est spécialisée en audit et le conseil et représente un acteur incontournable en matière fiscal sur le plan national.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--// [END] Page Hero //-->
    <section class="about-area">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                    <div class="about-text-box">
                        <div class="sec-title" id="21">
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                    data-target="#editContent" data-id="21">
                                Edit
                            </button>
                            <p class="title2">Qui sommes nous</p>
                            <h1 class="title1">A Propos de notre compagnie</h1>
                            <div class="border left"></div>
                        </div>
                        <div class="inner-content">
                            <h3>FIZAZI & ASSOCIÉS est une société à responsabilité limitée créée en 1999.</h3>
                            <div class="text">
                                <p>
                                    Elle est spécialisée en audit et le conseil et représente un acteur incontournable en matière fiscal sur le plan national. Doté d’une équipe composée d’experts-comptables DPLE, d’experts-comptables mémorialistes et d’experts comptables stagiaires sous l’encadrement attentif de l’Expert-Comptable DPLE et Commissaire aux comptes M. Khalid Fizazi, associé-gérant, le cabinet FIZAZI & ASSOCIÉS exécute ses missions avec minutie dans le strict respect des normes marocaines d’audit et garantit à ses clients, la qualité de sa signature.

                                    <br /><br />

                                    Le cabinet FIZAZI & ASSOCIÉS est reconnu pour sa dextérité dans l’accompagnement des clients qui sollicitent son service lors des procédures de vérification fiscale par le fisc. L’associé-gérant, M. Khalid Fizazi est un fiscaliste aguerri maîtrisant parfaitement les textes fiscaux et la procédure fiscale marocaine pour avoir mené avec brio un nombre conséquent de dossiers de contrôle fiscal.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                    <div class="company-featured-box clearfix">
                        <ul>
                            <li class="bg-one">
                                <div class="single-box" id="9">
                                    <button type="button" class="btn btn-primary" data-toggle="modal"
                                            data-target="#editContent" data-id="9">
                                        Edit
                                    </button>
                                    <div class="icon-holder">
                                        <span class="icon-pen"></span>
                                    </div>
                                    <div class="title-holder">
                                        <h3 class="title1">Intérêt du client<br> 2000 projets.</h3>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="single-box" id="10">
                                    <button type="button" class="btn btn-primary" data-toggle="modal"
                                            data-target="#editContent" data-id="10">
                                        Edit
                                    </button>
                                    <div class="icon-holder">
                                        <span class="icon-people-2"></span>
                                    </div>
                                    <div class="title-holder">
                                        <h3 class="title1">Écoute active<br> prêt à vous aider.</h3>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="single-box" id="11">
                                    <button type="button" class="btn btn-primary" data-toggle="modal"
                                            data-target="#editContent" data-id="11">
                                        Edit
                                    </button>
                                    <div class="icon-holder">
                                        <span class="icon-smile"></span>
                                    </div>
                                    <div class="title-holder">
                                        <h3 class="title1">Professionnalisme<br> satisfait de nous.</h3>
                                    </div>
                                </div>
                            </li>
                            <li class="blue-bg">
                                <div class="single-box" id="12">
                                    <button type="button" class="btn btn-primary" data-toggle="modal"
                                            data-target="#editContent" data-id="12">
                                        Edit
                                    </button>
                                    <div class="icon-holder">
                                        <span class="icon-medal"></span>
                                    </div>
                                    <div class="title-holder">
                                        <h3 class="title1">Respect des délais<br> meilleur service.</h3>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!--// [END] About //-->

    <!--// [BEGIN] History Area //-->
    <section class="history-area">
        <button type="button" class="btn btn-primary" data-toggle="modal"
                data-target="#createHistoire">
            ADD
        </button>
        <div class="container">
            <div class="sec-title" id="22">
                <button type="button" class="btn btn-primary" data-toggle="modal"
                        data-target="#editContent" data-id="22">
                    Edit
                </button>
                <p class="title2">Lorem ipsum dolor set</p>
                <h1 class="title1">Notre histoire en mots</h1>
                <div class="border left"></div>
            </div>
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                    <div class="history-box">
                        <div class="history-carousel">

                        @foreach($histoires as $h)
                            <!--// [BEGIN] Date //-->
                            <div class="single-item">
                                <div class="single-history leftside">
                                    <div class="date-box">
                                        <div class="box">
                                            <div class="content">
                                                <h5>{{$h->date}}</h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="img-holder">
                                        <img src="{{asset('cms_asset/assets/media/'.$h->image)}}" alt="">
                                        <div class="overlay-style-box"></div>
                                        <div class="overlay-content-box">
                                            <h3>{{$h->title}}</h3>
                                            <p>{{$h->description}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--// [END] Date //-->
                        @endforeach

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!--// [END] History Area //-->

    <!--// [BEGIN] Firm Values //-->
    <section class="advantages-area">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="advantages">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                <div class="text-holder" id="23">
                                    <div class="sec-title" >
                                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                                data-target="#editContent" data-id="23">
                                            Edit
                                        </button>
                                        <p class="title2">Meilleur de nous</p>
                                        <h1 class="title1">Nos Valeurs</h1>
                                        <div class="border center"></div>
                                    </div>
                                    <div class="text">
                                        <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit
                                            .<br />Aenean
                                            euismod bibendum laoreet</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                <div class="advantages-box">
                                    <ul>

                                        <li>
                                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                                    data-target="#editValue" data-id="1">
                                                Edit
                                            </button>
                                            <div class="icon-box">
                                                <span class="icon-communication"></span>
                                            </div>
                                            <div class="text-box" id="value_1">
                                                <h3 class="name">Professionnalisme</h3>
                                                <p class="description"> Nos collaborateurs sont emplis de
                                                    professionnalisme dans leur
                                                    gestion des différentes missions qu’ils sont...</p>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="icon-box">
                                                <span class="icon-money-1"></span>
                                            </div>
                                            <div class="text-box" id="value_2">
                                                <h3 class="name">Professionnalisme</h3>
                                                <p class="description"> Nos collaborateurs sont emplis de
                                                    professionnalisme dans leur
                                                    gestion des différentes missions qu’ils sont...</p>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="icon-box">
                                                <span class="icon-money-1"></span>
                                            </div>
                                            <div class="text-box" id="value_3">
                                                <h3 class="name">Professionnalisme</h3>
                                                <p class="description"> Nos collaborateurs sont emplis de
                                                    professionnalisme dans leur
                                                    gestion des différentes missions qu’ils sont...</p>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="icon-box">
                                                <span class="icon-money-1"></span>
                                            </div>
                                            <div class="text-box" id="value_4">
                                                <h3 class="name">Professionnalisme</h3>
                                                <p class="description"> Nos collaborateurs sont emplis de
                                                    professionnalisme dans leur
                                                    gestion des différentes missions qu’ils sont...</p>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="icon-box">
                                                <span class="icon-money-1"></span>
                                            </div>
                                            <div class="text-box" id="value_5">
                                                <h3 class="name">Professionnalisme</h3>
                                                <p class="description"> Nos collaborateurs sont emplis de
                                                    professionnalisme dans leur
                                                    gestion des différentes missions qu’ils sont...</p>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="icon-box">
                                                <span class="icon-money-1"></span>
                                            </div>
                                            <div class="text-box" id="value_6">
                                                <h3 class="name">Professionnalisme</h3>
                                                <p class="description"> Nos collaborateurs sont emplis de
                                                    professionnalisme dans leur
                                                    gestion des différentes missions qu’ils sont...</p>
                                            </div>
                                        </li>



                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--// [END] Firm Values //-->

    <!--// [BEGIN] Industries //-->
    <section class="latest-project-area">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="sec-title float-center" id="13">
                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#editContent" data-id="13">
                            Edit
                        </button>
                        <p class="title2">Dans divers secteurs d’activité</p>
                        <h1 class="title1">Industries couvertes</h1>
                        <div class="border center"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="project-content masonary-layout filter-layout clearfix">


        @foreach($secteurs as $sect)
            <!--// [BEGIN] Industry Item //-->
            <div class="single-item span-width-5 filter-item">
                <div class="single-project-item">
                    <div class="img-holder">
                        <img src="{{asset('cms_asset/assets/media/'.$sect->image)}}" alt="">
                        <div class="overlay-style-one">
                            <div class="box">
                                <div class="content">
                                    <div class="inner-content">
                                        <span>{{$sect->name}}</span>
                                        <h3><a href="{{route('show_secteur', $sect->id)}}">{{ str_limit
                                        ($sect->description, $limit =
                                        30, $end = '...')}}</a></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--// [END] Industry Item //-->
            @endforeach






        </div>
    </section>
    <!--// [END] Industries //-->

    <!--// [BEGIN] Team //-->
    <section class="team-area">
        <div class="container">
            <button type="button" class="btn btn-primary" data-toggle="modal"
                    data-target="#editContent" data-id="24">
                Edit
            </button>
            <div class="sec-title" id="24">

                <p class="title2">Derrière notre succès</p>
                <h1 class="title1">Rencontrez notre équipe</h1>
                <div class="border left"></div>
            </div>
            <button type="button" class="btn btn-primary" data-toggle="modal"
                    data-target="#createMembre">
                ADD
            </button>
            <div class="row">
                <div class="col-md-12">
                    <div class="team-carousel owl-dot-style-one">
                @foreach($membres as $m)
                        <!--// [BEGIN] Member //-->
                        <div class="single-team-member text-center">
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                    data-target="#editMembre" data-id="{{$m->id}}" data-name="{{$m->name}}"
                                    data-email="{{$m->email}}" data-position="{{$m->position}}"
                                    data-facebook="{{$m->facebook}}" data-twitter="{{$m->twitter}}"
                                    data-linkedin="{{$m->linkedin}}">
                                Edit
                            </button>
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                    data-target="#editImage_Membre" data-id="{{$m->id}}">
                                Image
                            </button>
                            <div class="inner-box">
                                <div class="img-holder">
                                    <img src="{{asset('cms_asset/assets/media/'.$m->image)}}" alt="">
                                    <div class="overlay-style-one">
                                        <div class="box">
                                            <div class="content">
                                                <ul>
                                                   @if($m->facebook) <li><a href="#"><i class="fa fa-facebook"
                                                                        aria-hidden="true"></i></a></li> @endif
                                                       @if($m->twitter) <li><a href="#"><i class="fa fa-twitter"
                                                                                     aria-hidden="true"></i></a></li>@endif
                                                       @if($m->linkedin) <li><a href="#"><i class="fa fa-linkedin"
                                                                                     aria-hidden="true"></i></a></li>@endif
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-holder">
                                <h3>{{$m->name}}</h3>
                                <span>{{$m->position}}</span>
                                <ul>
                                    <li><span class="icon-multimedia"></span>{{$m->email}}</li>
                                </ul>
                            </div>
                        </div>
                        <!--// [END] Member //-->

                    @endforeach

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--// [END] Team //-->

    <div class="modal fade" id="editImage_Membre" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h3 class="modal-title" id="lineModalLabel">My Modal</h3>
                </div>
                <div class="modal-body">
                    <form action="{{route('edit_image_membre')}}" method="post"
                          class="dropzone" id="edit-image-membre">
                        {{csrf_field()}}
                        <input type="hidden" name="id" class="id">
                    </form>


                </div>

            </div>
        </div>
    </div>






    @include('includes.modal_edit_content')
    @include('includes.modal_edit_value')
    @include('includes.modal_histoire')
    @include('includes.modal_equipe')


@stop