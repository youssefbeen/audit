@extends('cms.app')

@section('content')


    <button type="button" class="btn btn-primary" data-toggle="modal"
            data-target="#createService">
        Create Service
    </button>

    @include('includes.modal_service')



    <!--// [BEGIN] Page Hero //-->
    <section class="breadcrumb-area" style="background-image: url(../assets/images/resources/breadcrumb-bg.jpg);">
        <button type="button" class="btn btn-primary" data-toggle="modal"
                data-target="#editContent" data-id=30>
            Edit
        </button>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumbs" id="30">
                        <h1 class="title1">Services</h1>
                        <p class="description">

                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--// [END] Page Hero //-->


    <section class="project-area-v3">
        <div class="container">
        @foreach ($services as $key => $s)
            <!--// [BEGIN] Services List //-->
            @if(!$key%2)
            <!--// [BEGIN] Service Item //-->
            <div class="single-modern-project">
                <div class="row">
                    <div class="col-xl-7 col-lg-6 col-md-12 col-sm-12">
                        <div class="img-holder">
                            <img src="{{asset('cms_asset/assets/media/'. $s->image)}}" alt="">
                        </div>
                    </div>
                    <div class="col-xl-5 col-lg-6 col-md-12 col-sm-12">
                        <div class="text-holder">
                            <h3>{{$s->name}}</h3>
                            <div class="text">
                                <p>{{$s->description}}</p>
                            </div>
                            <a class="know-more" href="{{route('show_service', $s->id)}}/">Découvrir </a>
                        </div>
                    </div>
                </div>
            </div>
            <!--// [END] Service Item //-->
            @else

            <!--// [BEGIN] Service Item //-->
            <div class="single-modern-project">
                <div class="row">

                    <div class="col-xl-5 col-lg-6 col-md-12 col-sm-12">
                        <div class="text-holder">
                            <h3>{{$s->name}}</h3>
                            <div class="text">
                                <p>{{$s->description}}</p>
                            </div>
                            <a class="know-more" href="{{route('show_service', $s->id)}}">Découvrir </a>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-6 col-md-12 col-sm-12">
                        <div class="img-holder">
                            <img src="{{asset('cms_asset/assets/media/'. $s->image)}}" alt="">
                        </div>
                    </div>

                </div>
            </div>
            <!--// [END] Service Item //-->
        @endif
            @endforeach

        </div>
    </section>
    <!--// [END] Services List //-->


@stop