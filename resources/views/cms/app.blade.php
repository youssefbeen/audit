<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Accueil - FIZAZI & ASSOCIÉS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    {{--<link rel="stylesheet" href="{{asset('cms_asset/assets/css/style.css')}}">--}}
    {{--    <link rel="stylesheet" href="{{asset('cms_asset/assets/css/responsive.css')}}">--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.css">
    <link rel="stylesheet" href="{{asset('cms_asset/assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('cms_asset/assets/css/responsive.css')}}">

    <link rel="stylesheet" href="{{asset('cms_asset/assets/css/cms.css')}}">

    <link rel="apple-touch-icon" sizes="180x180" href="">
    <link rel="icon" type="image/png" href="" sizes="32x32">
    <link rel="icon" type="image/png" href="" sizes="16x16">

    <!-- Fixing Internet Explorer-->
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="{{asset('cms_asset/assets/js/html5shiv.js')}}"></script>
    <![endif]-->

</head>

<body>
<div class="boxed_wrapper">

    @include('includes.head_cms')
    @include('includes.modal_nav')
    <!--// [BEGIN] Menu //-->
    @include('includes.nav_cms', ['services' => App\Service::all()])

@yield('content')



@include('includes.modal_edit_info')
        @include('includes.modal_edit_content')
@include('includes.footer_cms')
</div>
    <!--// [BEGIN] Scroll to top //-->
    <div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-angle-up"></span></div>
    <!--// [END] Scroll to top //-->



    <!-- main jQuery -->
    <script src="{{asset('cms_asset/assets/js/jquery.js')}}"></script>
    <!-- Wow Script -->
    <script src="{{asset('cms_asset/assets/js/wow.min.js')}}"></script>
    <!-- bootstrap -->
    <script src="{{asset('cms_asset/assets/js/bootstrap.min.js')}}"></script>
    <!-- bx slider -->
    <script src="{{asset('cms_asset/assets/js/jquery.bxslider.min.js')}}"></script>
    <!-- count to -->
    <script src="{{asset('cms_asset/assets/js/jquery.countTo.js')}}"></script>
    <!-- owl carousel -->
    <script src="{{asset('cms_asset/assets/js/owl.js')}}"></script>
    <script src="{{asset('cms_asset/assets/js/owl.carousel.min.js')}}"></script>
    <!-- validate -->
    <script src="{{asset('cms_asset/assets/js/validation.js')}}"></script>
    <!-- mixit up -->
    <script src="{{asset('cms_asset/assets/js/jquery.mixitup.min.js')}}"></script>
    <!-- easing -->
    <script src="{{asset('cms_asset/assets/js/jquery.easing.min.js')}}"></script>
    <!-- gmap helper -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHzPSV2jshbjI8fqnC_C4L08ffnj5EN3A')}}"></script>
    <!--gmap script-->
    <script src="{{asset('cms_asset/assets/js/gmaps.js')}}"></script>
    <script src="{{asset('cms_asset/assets/js/map-helper.js')}}"></script>
    <!-- video responsive script -->
    <script src="{{asset('cms_asset/assets/js/jquery.fitvids.js')}}"></script>
    <!-- jQuery ui js -->
    <script src="{{asset('cms_asset/assets/jquery-ui-1.11.4/jquery-ui.js')}}"></script>
    <!-- Language Switche  -->
    <script src="{{asset('cms_asset/assets/language-switcher/jquery.polyglot.language.switcher.js')}}"></script>
    <!-- fancy box -->
    <script src="{{asset('cms_asset/assets/js/jquery.fancybox.pack.js')}}"></script>
    <script src="{{asset('cms_asset/assets/js/jquery.appear.js')}}"></script>
    <!-- isotope script-->
    <script src="{{asset('cms_asset/assets/js/isotope.js')}}"></script>
    <script src="{{asset('cms_asset/assets/js/jquery.prettyPhoto.js')}}"></script>
    <!-- jQuery timepicker js -->
    <script src="{{asset('cms_asset/assets/timepicker/timePicker.js')}}"></script>
    <!-- Bootstrap select picker js -->
    <script src="{{asset('cms_asset/assets/bootstrap-sl-1.12.1/bootstrap-select.js')}}"></script>
    <script src="{{asset('cms_asset/assets/html5lightbox/html5lightbox.js')}}"></script>
    <!--Color Switcher-->
    <script src="{{asset('cms_asset/assets/js/color-settings.js')}}"></script>

    <!-- revolution slider js -->
    <script src="{{asset('cms_asset/assets/revolution/js/jquery.themepunch.tools.min.js')}}"></script>
    <script src="{{asset('cms_asset/assets/revolution/js/jquery.themepunch.revolution.min.js')}}"></script>
    <script src="{{asset('cms_asset/assets/revolution/js/extensions/revolution.extension.actions.min.js')}}"></script>
    <script src="{{asset('cms_asset/assets/revolution/js/extensions/revolution.extension.carousel.min.js')}}"></script>
    <script src="{{asset('cms_asset/assets/revolution/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
    <script src="{{asset('cms_asset/assets/revolution/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
    <script src="{{asset('cms_asset/assets/revolution/js/extensions/revolution.extension.migration.min.js')}}"></script>
    <script src="{{asset('cms_asset/assets/revolution/js/extensions/revolution.extension.navigation.min.js')}}"></script>
    <script src="{{asset('cms_asset/assets/revolution/js/extensions/revolution.extension.parallax.min.js')}}"></script>
    <script src="{{asset('cms_asset/assets/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
    <script src="{{asset('cms_asset/assets/revolution/js/extensions/revolution.extension.video.min.js')}}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>


    <!-- thm custom script -->
    <script src="{{asset('cms_asset/assets/js/custom.js')}}"></script>
    <script src="{{asset('cms_asset/assets/js/cms.js')}}"></script>



</body>
</html>