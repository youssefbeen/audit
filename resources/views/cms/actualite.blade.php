@extends('cms.app')

@section('content')


    <!--// [BEGIN] Page Hero //-->
    <section class="breadcrumb-area" style="background-image: url(/cms_asset/assets/images/resources/breadcrumb-bg
    .jpg);">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumbs">
                        <h1>{{$article->title}}</h1>
                        <p>
                           {{str_limit($article->description, $limit = 70, $end = '...')}}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--// [END] Page Hero //-->

    <!--// [BEGIN] Article //-->
    <section id="blog-area" class="blog-single-area">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                    <div class="blog-post">

                        <div class="single-blog-post">
                            <div class="img-holder">
                                <img src="{{asset('cms_asset/assets/media/'.$article->image)}}" alt="">
                            </div>
                            <div class="text-holder">
                                <div class="top clearfix">
                                    <div class="meta-box">
                                        <ul class="meta-info">
                                            <li><a href="#">Par {{$article->user['name']}}</a></li>
                                            <li><a href="#">{{$article->categories[0]['name']}}</a></li>
                                            <li><a href="#">{{$article->created_at->diffForHumans()}}</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="text">
                                    <h3 class="blog-title">{{$article->title}}</h3>
                                    <p>
                                        {{$article->description}}
                                    </p>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>


            </div>
        </div>

        <div class="container">
            <div class="blog-post">
                <div class="row">

                    <div class="col-xl-12">
                        <div class="sec-title float-center">
                            <p>Lorem ipsum dolor set amet</p>
                            <h1>Dans la même catégorie</h1>
                            <div class="border center"></div>
                        </div>
                    </div>
            @foreach($article->categories[0]->actualites as $a)


                    <!--// [BEGIN] Post Item //-->
                    <div class="col-xl-4">
                        <div class="single-blog-post">
                            <div class="img-holder">
                                <img src="{{asset('cms_asset/assets/media/'.$a->image)}}" alt="Awesome Image">
                                <div class="overlay-style-one">
                                    <div class="box">
                                        <div class="content">
                                            <a href="javascript:void(0)"><span class="border"></span>Lire la suite</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-holder">
                                <div class="date-box clearfix">
                                    <div class="left float-left">
                                        <div class="month-year">
                                            <h4>{{$a->created_at->diffForHumans()}}</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="top clearfix">
                                    <div class="meta-box">
                                        <ul class="meta-info">
                                            <li><a href="#">Par {{$a->user['name']}}</a></li>
                                            <li><a href="#">{{$a->categories[0]['name']}}</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="text">
                                    <h3><a class="blog-title" href="javascript:void(0)">{{$a->title}}</a></h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--// [END] Post Item //-->
            @endforeach

                </div>
            </div>
        </div>
    </section>
    <!--// [END] Article //-->

@stop