@extends('cms.app')

@section('content')

    <!--// [BEGIN] Page Hero //-->
    <section class="breadcrumb-area" style="background-image: url(../assets/images/resources/breadcrumb-bg.jpg);">

        <button type="button" class="btn btn-primary" data-toggle="modal"
                data-target="#editContent" data-id="32">
            Edit
        </button>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumbs" id="32">
                        <h1 class="title1">Actualités</h1>
                        <p class="description">
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--// [END] Page Hero //-->
    <button type="button" class="btn btn-primary" data-toggle="modal"
            data-target="#addCategory">
        Add Category
    </button>
    <button type="button" class="btn btn-primary" data-toggle="modal"
            data-target="#addArticle">
        Add Article
    </button>
    <!--// [BEGIN] Actualites List //-->
    <section id="blog-area" class="blog-large-area">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                    <div class="blog-post">

                @foreach($actualites as $a)
                        <!--// [BEGIN] Post //-->
                        <div class="single-blog-post">
                            <div class="img-holder">
                                <img src="{{asset('cms_asset/assets/media/'.$a->image)}}" alt="">
                                <div class="overlay-style-one">
                                    <div class="box">
                                        <div class="content">
                                            <a href="post/">{{ str_limit($a->description, $limit = 70, $end = '...')
                                            }}<span
                                                        class="border"></span>Lire la suite</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-holder">
                                <div class="top clearfix">
                                    <div class="meta-box float-left">
                                        <ul class="meta-info">
                                            <li><a href="#">Par {{$a->user['name']}}</a></li>
                                            <li><a href="#">{{$a->categories[0]['name']}}</a></li>
                                            <li><a href="#">{{$a->created_at->diffForHumans()}}</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="text">
                                    <h3><a class="blog-title" href="{{route('show_article', $a->id)
                                    }}">{{$a->title}}</a></h3>
                                    <p>{{str_limit($a->description, $limit = 150, $end = '...')}}</p>
                                </div>
                            </div>
                        </div>
                        <!--// [END] Post //-->
                       @endforeach
                        <!--// [BEGIN] Pagination //-->
                        {{--<div class="row">--}}
                            {{--<div class="col-md-12">--}}
                                {{--<ul class="post-pagination text-center">--}}
                                    {{--<li><a href="#"><i class="fa fa-caret-left" aria-hidden="true"></i></a></li>--}}
                                    {{--<li class="active"><a href="#">1</a></li>--}}
                                    {{--<li><a href="#">2</a></li>--}}
                                    {{--<li><a href="#">3</a></li>--}}
                                    {{--<li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i></a></li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <!--// [END] Pagination //-->

                    </div>
                </div>

            </div>
        </div>
    </section>
    <!--// [END] Actualites List //-->



    <!-- Modal -->
    <div class="modal fade" id="addCategory" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h3 class="modal-title" id="lineModalLabel">My Modal</h3>
                </div>
                <div class="modal-body">

                    <!-- content goes here -->
                    <form action="{{route('add_category')}}" method="post">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="name"> Name </label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-default submit sub-drop">Submit</button>
                        </div>
                    </form>

                </div>

            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="addArticle" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h3 class="modal-title" id="lineModalLabel">My Modal</h3>
                </div>
                <div class="modal-body">

                    <!-- content goes here -->
                    <form action="{{route('add_article')}}" method="post" class="dropzone" id="create-article">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="title"> Title </label>
                            <input type="text" class="form-control" id="title" name="title" placeholder="Title">
                        </div>
                        <div class="form-group">
                            <label for="title"> Description </label>
                            <textarea type="text" class="form-control" id="description" name="description"></textarea>
                        </div>

                        <div class="col-md-6 c-select u-mb-xsmall">
                            <select class="form-control" name="category_id" id="category_id" type="text"
                                    placeholder="Select">
                                <option value="">Category</option>
                                @foreach($categories as $c)
                                    <option value="{{$c->id}}">{{$c->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="dz-message"></div>
                        <div class="upl-box">
                            Upload Media
                        </div>
                        <div class="dz-preview dz-file-preview"></div>

                        <input type="hidden" name="user_id" value="{{Auth::user()->id}}">

                        <div class="form-group">
                            <button type="submit" class="btn btn-default submit sub-drop">Submit</button>
                        </div>
                    </form>

                </div>

            </div>
        </div>
    </div>

@stop