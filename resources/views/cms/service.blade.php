@extends('cms.app')

@section('content')

    <section class="breadcrumb-area" style="background-image: url(../../assets/images/resources/breadcrumb-bg.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumbs">
                        <h1>{{$service->name}}</h1>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--// [END] Page Hero //-->

    <!--// [BEGIN] Service Details //-->
    <section id="single-service-area">
        <div class="container">
            <div class="row">

                <button type="button" class="btn btn-primary" data-toggle="modal"
                        data-target="#editService">
                    Edit
                </button>
                <button type="button" class="btn btn-primary" data-toggle="modal"
                        data-target="#editImageService">
                    Image
                </button>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 pull-left">
                    <div class="content-box">
                        <div class="top-content">
                            <div class="row">
                                <div class="col-xl-5">
                                    <div class="img-holder">
                                        <img src="{{asset('cms_asset/assets/media/'. $service->image)}}" alt="">
                                    </div>
                                </div>
                                <div class="col-xl-7">
                                    <div class="text-holder">
                                        <div class="inner-content">
                                            <div class="sec-title">
                                                <h1>{{$service->name}}</h1>
                                                <div class="border left"></div>
                                            </div>
                                            <div class="text">
                                                {{$service->description}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--End top content-->

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--// [END] Service Details //-->

    <!--// [BEGIN] Firm Values //-->
    <section class="advantages-area">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="advantages">
                        <div class="row">
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                    data-target="#createAspect">
                                Create
                            </button>
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                        data-target="#editContent" data-id="34">
                                    Edit
                                </button>
                                <div class="text-holder" id="34">
                                    <div class="sec-title" >
                                        <p class="title2">Lorem ipsum dolor</p>
                                        <h1 class="title1">Explorez notre secteur</h1>
                                        <div class="border center"></div>
                                    </div>
                                    <div class="text">
                                        <p class="description">Nos principaux services dans le cadre de l’expertise
                                            comptable
                                            couvrent les aspects suivants</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                <div class="advantages-box">
                                    <ul>
                                    @foreach($service->aspects as $a)
                                        <li>
                                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                                    data-target="#editAspect" data-id="{{$a->id}}">
                                                Edit
                                            </button>
                                            <div class="icon-box">
                                                <img width="100px" height="100px" src="{{asset
                                                ('cms_asset/assets/media/'.$a->image)}}" style="border-radius: 50%;"
                                                     alt="">
                                            </div>
                                            <div class="text-box" id="aspect_{{$a->id}}">
                                                <h3 class="name"></h3>
                                                <p class="description"></p>
                                            </div>
                                        </li>
                                    @endforeach



                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--// [END] Firm Values //-->

    <!--// [BEGIN] Testimonials //-->
    <section class="testimonial-section" style="background-image:url(../../assets/images/parallax-background/testimonial-section-bg.jpg);">
        <div class="container">
            <div class="title text-center">
                <h1>Nous avons fait confiance de plus de 100 clients</h1>
                <span>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span>
            </div>
            <div class="testimonial-outer">
                <div class="quote-icon text-center">
                    <span class="icon-school"></span>
                </div>
                <!--Client Testimonial Carousel-->
                <button type="button" class="btn btn-primary" data-toggle="modal"
                        data-target="#createTestamonials">
                    Create
                </button>
                <div class="client-testimonial-carousel">
                    @foreach($service->testamonials as $t)

                    <div class="testimonial-block-one">
                        <div class="inner-box">
                            <div class="text">
                                {{$t->description}}
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>

                <!--// [BEGIN] Thumbs Carousel //-->
                <div class="client-thumb-outer">
                    <div class="client-thumbs-carousel owl-carousel owl-theme">
                        @foreach($service->testamonials as $t)
                        <div class="thumb-item">
                            <figure class="thumb-box"><img src="{{asset('cms_asset/assets/media/'.$t->image)}}"
                                                           alt=""></figure>
                            <div class="thumb-content">
                                <h3>{{$t->name}}</h3>
                                <div class="designation">{{$t->entreprise}}</div>
                            </div>
                        </div>
                        @endforeach

                    </div>
                </div>
                <!--// [END] Thumbs Carousel //-->

            </div>
        </div>
    </section>
    <!--// [END] Testimonials //-->

    @include('includes.nouvelle')


    <!-- Modal -->
    <div class="modal fade" id="editService" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h3 class="modal-title" id="lineModalLabel">My Modal</h3>
                </div>
                <div class="modal-body">

                    <!-- content goes here -->
                    <form action="{{route('edit_service', $service->id)}}" method="post">
                        {{csrf_field()}}
                        <div class="form-group" id="name">
                            <label for="name">Titre </label>
                            <input type="text" class="form-control" id="name" name="name" value="{{$service->name}}">
                        </div>

                        <div class="form-group" id="description_div">
                            <label for="description">Description</label>
                            <textarea type="text" class="form-control" id="description"
                                      name="description">{{$service->description}}</textarea>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-default submit sub-drop">Submit</button>
                        </div>
                    </form>

                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="editImageService" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h3 class="modal-title" id="lineModalLabel">My Modal</h3>
                </div>
                <div class="modal-body">


                    <form action="{{route('update_image_service', $service->id)}}"
                          class="dropzone" id="edit-image-secteur">
                        {{csrf_field()}}
                    </form>


                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="createTestamonials" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h3 class="modal-title" id="lineModalLabel">My Modal</h3>
                </div>
                <div class="modal-body">

                    <!-- content goes here -->
                    <form action="{{route('store_testamonials')}}" method="post" class="dropzone"
                          id="create-testamonials">
                        {{csrf_field()}}

                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea type="text" class="form-control" id="description" placeholder="Description"
                                      name="description"></textarea>
                        </div>

                        <div class="form-group" >
                            <label for="name">Titre </label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Nom Et Prenom">
                        </div>
                        <div class="form-group">
                            <label for="entreprise">Entreprise </label>
                            <input type="text" class="form-control" id="entreprise" name="entreprise"
                                   placeholder="Nom Entreprise">
                        </div>


                        <input type="hidden" name="service_id" value="{{$service->id}}">
                        <div class="dz-message"></div>
                        <div class="upl-box">
                            Upload Media
                        </div>
                        <div class="dz-preview dz-file-preview"></div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-default submit sub-drop">Submit</button>
                        </div>
                    </form>

                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="createAspect" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h3 class="modal-title" id="lineModalLabel">My Modal</h3>
                </div>
                <div class="modal-body">

                    <!-- content goes here -->
                    <form action="{{route('store_aspect')}}" method="post" class="dropzone"
                          id="create-aspect">
                        {{csrf_field()}}


                        <div class="form-group" >
                            <label for="name">Name </label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea type="text" class="form-control" id="description" placeholder="Description"
                                      name="description"></textarea>
                        </div>

                        <input type="hidden" name="service_id" value="{{$service->id}}">
                        {{--<div class="dz-message"></div>--}}
                        <div id="dropzonePreview" class="dz-default dz-message upl-box col-md-12">
                            Upload Media
                        </div>
                        {{--<div class="upl-box">--}}
                            {{--Upload Media--}}
                        {{--</div>--}}
                        {{--<div class="dz-preview dz-file-preview"></div>--}}
                        {{--<div class="form-group">--}}
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-default submit sub-drop">Submit</button>
                        </div>
                    </form>

                </div>

            </div>
        </div>
    </div>



    <!-- Modal -->
    <div class="modal fade" id="editAspect" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h3 class="modal-title" id="lineModalLabel">My Modal</h3>
                </div>
                <div class="modal-body">

                    <!-- content goes here -->
                    <form>
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="name">Titre </label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea type="text" class="form-control" id="description" placeholder="Description"
                                      name="description"></textarea>
                        </div>

                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="visible" value="1" id="visible"> Visible
                            </label>
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>

                </div>

            </div>
        </div>
    </div>




@stop