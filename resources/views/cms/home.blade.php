@extends('cms.app')

@section('content')
    <!--// [BEGIN] Hero Slider //-->
    <section class="rev_slider_wrapper back">
        <div id="slider1" class="rev_slider"  data-version="5.0">
            <button type="button" class="btn btn-primary" data-toggle="modal"
                    data-target="#modalHero">
                ADD
            </button>

            <ul>
                @foreach($heroes as $h)

                <li data-transition="fade" id="hero_{{$h->id}}">

                    <img alt="" width="1920" height="780"
                         data-bgposition="top
                    center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1" id="imgi">
                    <div class="tp-caption  tp-resizeme"
                         data-x="left" data-hoffset="0"
                         data-y="top" data-voffset="195"
                         data-transform_idle="o:1;"
                         data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;"
                         data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                         data-mask_in="x:[100%];y:0;s:inherit;e:inherit;"
                         data-splitin="none"
                         data-splitout="none"
                         data-responsive_offset="on"
                         data-start="1500">
                        <div class="slide-content left-slide">
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                    data-target="#editHero" data-id="{{$h->id}}">
                                Edit
                            </button>
                            <h3 class="title2"></h3>
                            <div class="big-title title1"></div>
                            <div class="text description"></div>
                            <div class="btns-box">
                                <a href="about.html" class="btn-one pdone boutton"></a>&ensp;
                            </div>
                        </div>
                    </div>

                </li>
                @endforeach
                {{--<li data-transition="fade">--}}
                    {{--<img src="assets/images/homes/slide2.jpg"  alt="" width="1920" height="780" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1">--}}

                    {{--<div class="tp-caption  tp-resizeme"--}}
                         {{--data-x="right" data-hoffset="0"--}}
                         {{--data-y="top" data-voffset="195"--}}
                         {{--data-transform_idle="o:1;"--}}
                         {{--data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;"--}}
                         {{--data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"--}}
                         {{--data-mask_in="x:[100%];y:0;s:inherit;e:inherit;"--}}
                         {{--data-splitin="none"--}}
                         {{--data-splitout="none"--}}
                         {{--data-responsive_offset="on"--}}
                         {{--data-start="1500">--}}
                        {{--<div class="slide-content right-slide">--}}
                            {{--<h3>Aide pour impôts ou taxes?</h3>--}}
                            {{--<div class="big-title">Success</div>--}}
                            {{--<div class="text">Notre équipe de conseillers est là pour vous servir.</div>--}}
                            {{--<div class="btns-box">--}}
                                {{--<a href="about.html" class="btn-one pdone">En Savoir plus</a>&ensp;--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</li>--}}
                {{--<li data-transition="fade" id="1">--}}
                    {{--<img src="assets/images/homes/slide3.jpg" alt="" width="1920" height="780" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1" >--}}

                    {{--<div class="tp-caption  tp-resizeme"--}}
                         {{--data-x="left" data-hoffset="0"--}}
                         {{--data-y="top" data-voffset="195"--}}
                         {{--data-transform_idle="o:1;"--}}
                         {{--data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;"--}}
                         {{--data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"--}}
                         {{--data-mask_in="x:[100%];y:0;s:inherit;e:inherit;"--}}
                         {{--data-splitin="none"--}}
                         {{--data-splitout="none"--}}
                         {{--data-responsive_offset="on"--}}
                         {{--data-start="1500">--}}
                        {{--<div class="slide-content left-slide" >--}}
                            {{--<h3 class="title2">Choisir une équipe</h3>--}}
                            {{--<div class="big-title title1">d'experts</div>--}}
                            {{--<div class="text description">C'est économiser temps et argent.</div>--}}
                            {{--<div class="btns-box">--}}
                                {{--<a href="about.html" class="btn-one pdone boutton">En Savoir plus</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                {{--</li>--}}
            </ul>
        </div>
    </section>
    <!--// [END] Hero Slider //-->

    <!--// [BEGIN] Choose Area  //-->
    <section class="choose-area">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                    <div class="inner-content parallax-bg-one" style="background-image:url(images/parallax-background/choose-bg.jpg);">
                        <div class="sec-title text-center" id="2">
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                    data-target="#editContent" data-id="2">
                                Edit
                            </button>
                            <h1 class="white-color title1">Pourquoi nous choisir</h1>
                            <div class="border center"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12">
                    <div class="featured-item-box">
                        <div class="row">

                            <!--// [BEGIN] Box //-->
                            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12" id="3">

                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                        data-target="#editContent" data-id="3">
                                    Edit
                                </button>
                                <div class="single-box text-center">
                                    <div class="icon-holder">
                                        <span class="icon-people"></span>
                                    </div>
                                    <h3 class="title1"></h3>
                                    <span class="title2">Lorem</span>
                                    <div class="overlay-content-box">
                                        <div class="icon-holder">
                                            <span class="icon-people"></span>
                                        </div>
                                        <h3 class="title1"></h3>
                                        <span class="title2">Lorem</span>
                                        <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit
                                            .</p>
                                        <div class="border"></div>
                                    </div>
                                </div>
                            </div>
                            <!--// [END] Box //-->

                            <!--// [BEGIN] Box //-->
                            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12" id="4">

                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                        data-target="#editContent" data-id="4">
                                    Edit
                                </button>
                                <div class="single-box text-center">
                                    <div class="icon-holder">
                                        <span class="icon-business"></span>
                                    </div>
                                    <h3 class="title1"></h3>
                                    <span class="title2">Lorem</span>
                                    <div class="overlay-content-box">
                                        <div class="icon-holder">
                                            <span class="icon-business"></span>
                                        </div>
                                        <h3 class="title1"></h3>
                                        <span class="title2">Lorem</span>
                                        <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit
                                            .</p>
                                        <div class="border"></div>
                                    </div>
                                </div>
                            </div>
                            <!--// [END] Box //-->

                            <!--// [BEGIN] Box //-->
                            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12" id="5">

                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                        data-target="#editContent" data-id="5">
                                    Edit
                                </button>
                                <div class="single-box text-center">
                                    <div class="icon-holder">
                                        <span class="icon-landscape"></span>
                                    </div>
                                    <h3 class="title1"></h3>
                                    <span class="title2">Lorem</span>
                                    <div class="overlay-content-box">
                                        <div class="icon-holder">
                                            <span class="icon-landscape"></span>
                                        </div>
                                        <h3 class="title1"></h3>
                                        <span class="title2">Lorem</span>
                                        <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit
                                            .</p>
                                        <div class="border"></div>
                                    </div>
                                </div>
                            </div>
                            <!--// [END] Box //-->

                            <!--// [BEGIN] Box //-->
                            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12" id="6">

                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                        data-target="#editContent" data-id="6">
                                    Edit
                                </button>
                                <div class="single-box text-center">
                                    <div class="icon-holder">
                                        <span class="icon-people-1"></span>
                                    </div>
                                    <h3 class="title1"></h3>
                                    <span class="title2">Lorem</span>
                                    <div class="overlay-content-box">
                                        <div class="icon-holder">
                                            <span class="icon-people-1"></span>
                                        </div>
                                        <h3 class="title1"></h3>
                                        <span class="title2">Lorem</span>
                                        <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit
                                            .</p>
                                        <div class="border"></div>
                                    </div>
                                </div>
                            </div>
                            <!--// [END] Box //-->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--// [END] Choose Area  //-->

    <!--// [BEGIN] Services //-->
    <section class="services-area">
        <div class="container" id="7">
            <div class="sec-title text-center">
                <button type="button" class="btn btn-primary" data-toggle="modal"
                        data-target="#editContent" data-id="7">
                    Edit
                </button>
                <p class="title2">À la hauteur de vos attentes.</p>
                <h1 class="title1">Nos services</h1>
                <div class="border center"></div>
                <span class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
            </div>
            <div class="row">
                <div class="col-xl-12">
                    <div class="service-carousel">

                    @foreach($services as $s)
                        <!--// [BEGIN] Service Item //-->
                        <div class="single-service-item">
                            <div class="img-holder">
                                <img src="{{asset('cms_asset/assets/media/'.$s->image)}}" alt="">
                                <div class="icon-box">
                                    <span class="icon-suitcase"></span>
                                </div>
                                <div class="overlay-style-one">
                                    <div class="box">
                                        <div class="content">
                                            <a href="javascript:void(0)"><i class="fa fa-link" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-holder">
                                <h3 class="title"><a href="service-single.html">{{$s->name}}</a></h3>
                                <p>
                                    {{str_limit($s->description, $limit = 150, $end = '...')}}
                                </p>
                                <a class="readmore boutton" href="service-single.html">Découvrir</a>
                            </div>
                        </div>
                        <!--// [END] Service Item //-->
                        @endforeach


                    </div>
                </div>
            </div>
        </div>

        <!--Start service about bg-->
        <div class="service-about-bg parallax-bg-one" style="background-image:url(images/parallax-background/service-about-bg.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="inner-content">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--End service about bg-->

    </section>
    <!--// [END] Services //-->

    <!--// [BEGIN] About //-->
    <section class="about-area">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                    <div class="about-text-box" id="8">
                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#editContent" data-id="8">
                            Edit
                        </button>
                        <div class="sec-title">
                            <p class="title2">Qui sommes nous</p>
                            <h1 class="title1">A Propos de notre compagnie</h1>
                            <div class="border left"></div>
                        </div>
                        <div class="inner-content">
                            <h3>FIZAZI & ASSOCIÉS est une société à responsabilité limitée créée en 1999.</h3>
                            <div class="text">
                                <p>Elle est spécialisée en audit et le conseil et représente un acteur incontournable en matière fiscal sur le plan national. Doté d’une équipe composée d’experts-comptables DPLE, d’experts-comptables ...</p>
                            </div>
                            <div class="bottom">
                                <div class="read-more-button">
                                    <a href="javascript:void(0)" class="boutton">Decouvrir plus</a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                    <div class="company-featured-box clearfix">
                        <ul>
                            <li class="bg-one">
                                <div class="single-box" id="9">
                                    <button type="button" class="btn btn-primary" data-toggle="modal"
                                            data-target="#editContent" data-id="9">
                                        Edit
                                    </button>
                                    <div class="icon-holder">
                                        <span class="icon-pen"></span>
                                    </div>
                                    <div class="title-holder">
                                        <h3 class="title1">Intérêt du client<br> 2000 projets.</h3>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="single-box" id="10">
                                    <button type="button" class="btn btn-primary" data-toggle="modal"
                                            data-target="#editContent" data-id="10">
                                        Edit
                                    </button>
                                    <div class="icon-holder">
                                        <span class="icon-people-2"></span>
                                    </div>
                                    <div class="title-holder">
                                        <h3 class="title1">Écoute active<br> prêt à vous aider.</h3>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="single-box" id="11">
                                    <button type="button" class="btn btn-primary" data-toggle="modal"
                                            data-target="#editContent" data-id="11">
                                        Edit
                                    </button>
                                    <div class="icon-holder">
                                        <span class="icon-smile"></span>
                                    </div>
                                    <div class="title-holder">
                                        <h3 class="title1">Professionnalisme<br> satisfait de nous.</h3>
                                    </div>
                                </div>
                            </li>
                            <li class="blue-bg">
                                <div class="single-box" id="12">
                                    <button type="button" class="btn btn-primary" data-toggle="modal"
                                            data-target="#editContent" data-id="12">
                                        Edit
                                    </button>
                                    <div class="icon-holder">
                                        <span class="icon-medal"></span>
                                    </div>
                                    <div class="title-holder">
                                        <h3 class="title1">Respect des délais<br> meilleur service.</h3>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!--// [END] About //-->

    <!--// [BEGIN] Industries //-->
    <section class="latest-project-area">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="sec-title float-center" id="13">
                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#editContent" data-id="13">
                            Edit
                        </button>
                        <p class="title2">Dans divers secteurs d’activité</p>
                        <h1 class="title1">Industries couvertes</h1>
                        <div class="border center"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="project-content masonary-layout filter-layout clearfix">


        @foreach($secteurs as $sect)
            <!--// [BEGIN] Industry Item //-->
                <div class="single-item span-width-5 filter-item">
                    <div class="single-project-item">
                        <div class="img-holder">
                            <img src="{{asset('cms_asset/assets/media/'.$sect->image)}}" alt="">
                            <div class="overlay-style-one">
                                <div class="box">
                                    <div class="content">
                                        <div class="inner-content">
                                            <span>{{$sect->name}}</span>
                                            <h3><a href="project-single.html">{{ str_limit($sect->description, $limit =
                                        30, $end = '...')}}</a></h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--// [END] Industry Item //-->
            @endforeach






        </div>
    </section>
    <!--// [END] Industries //-->

    @include('includes.nouvelle')

    <!--// [BEGIN] CTA //-->

    <!--// [END] CTA //-->













    <!-- Modal -->
    <div class="modal fade" id="modalHero" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h3 class="modal-title" id="lineModalLabel">My Modal</h3>
                </div>
                <div class="modal-body">

                    <!-- content goes here -->
                    <form action="{{route('store_hero')}}" method="post" id="form_edit" class="dropzone">
                        {{csrf_field()}}
                        <div class="form-group" id="titre1">
                            <label for="title1">Titre </label>
                            <input type="text" class="form-control title1" id="tite1" name="title1" placeholder="Titre">
                        </div>
                        <div class="form-group" id="titre2">
                            <label for="title2">Sous titre</label>
                            <input type="text" class="form-control" name="title2" id="title2" placeholder="Sous Titre">
                        </div>
                        <div class="form-group" id="description_div">
                            <label for="description">Description</label>
                            <input type="text" class="form-control" name="description" id="description"
                                   placeholder="Description">
                        </div>
                        <div class="dz-message"></div>
                        <div class="upl-box">
                            Upload Media
                        </div>
                        <div class="dz-preview dz-file-preview"></div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-default submit sub-drop">Submit</button>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="editHero" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h3 class="modal-title" id="lineModalLabel">My Modal</h3>
                </div>
                <div class="modal-body">

                    <!-- content goes here -->
                    <form action="{{route('store_hero')}}" method="post" id="form_edit">
                        {{csrf_field()}}
                        <div class="form-group" id="titre1">
                            <label for="title1">Titre </label>
                            <input type="text" class="form-control title1" id="tite1" name="title1" placeholder="Titre">
                        </div>
                        <div class="form-group" id="titre2">
                            <label for="title2">Sous titre</label>
                            <input type="text" class="form-control" name="title2" id="title2" placeholder="Sous Titre">
                        </div>
                        <div class="form-group" id="description_div">
                            <label for="description">Description</label>
                            <input type="text" class="form-control" name="description" id="description"
                                   placeholder="Description">
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-default submit sub-drop">Submit</button>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>


@stop