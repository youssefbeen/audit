<!--// [BEGIN] CTA //-->
<section class="slogan-area">
    <div class="container">
        <div class="row">
            <div class="col-xl-12" id="15">
                <button type="button" class="btn btn-primary" data-toggle="modal"
                        data-target="#editContent" data-id="15">
                    Edit
                </button>
                <div class="inner-content">
                    <div class="title float-left">
                        <h3 class="title1"></h3>
                    </div>
                    <div class="button float-right">
                        <a href="javascript:void(0)" class="boutton"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--// [END] CTA //-->

<!--// [BEGIN] Footer //-->
<footer class="footer-area">
    <div class="container inner-content">
        <div class="row">

            <!--// [BEGIN] Bloc //-->
            <div class="col-xl-4 col-lg-3 col-md-6 col-sm-12">
                <div class="single-footer-widget">
                    <button type="button" class="btn btn-primary" data-toggle="modal"
                            data-target="#editInfos">
                        Edit
                    </button>
                    <div class="our-info">
                        <div class="footer-logo">
                            <a href="javascript:void(0)">
                                <img src="../../assets/images/footer/footer-logo.png" alt="Awesome Logo">
                            </a>
                        </div>
                        <div class="text">
                            <p class="adresse">Lot Y5, Secteur 14, Avenue Addolb,<br />Hay Ryad, Rabat, Maroc</p>
                            <p class="tel">Phone :  +212 5 37 71 63 98</p>
                            <p class="fax"></p>
                        </div>
                    </div>
                </div>
            </div>
            <!--// [END] Bloc //-->

            <!--// [BEGIN] Bloc //-->
            <div class="col-xl-2 col-lg-4 col-md-6 col-sm-12">
                <div class="single-footer-widget martop martop-50">
                    <div class="sec-title">
                        <h1 class="white-color">Cabinet</h1>
                    </div>
                    <ul class="usefull-links">
                        <li><a href="javascript:void(0)"><i class="fa fa-angle-right" aria-hidden="true"></i>A Propos</a></li>
                        <li><a href="javascript:void(0)"><i class="fa fa-angle-right" aria-hidden="true"></i>Secteurs couvertes</a></li>
                        <li><a href="javascript:void(0)"><i class="fa fa-angle-right" aria-hidden="true"></i>Clients</a></li>
                        <li><a href="javascript:void(0)"><i class="fa fa-angle-right" aria-hidden="true"></i>Carrières</a></li>
                        <li><a href="javascript:void(0)"><i class="fa fa-angle-right" aria-hidden="true"></i>Contact</a></li>



                    </ul>
                </div>
            </div>
            <!--// [BEGIN] Bloc //-->

            <!--// [BEGIN] Bloc //-->
            <div class="col-xl-2 col-lg-4 col-md-6 col-sm-12">
                <div class="single-footer-widget martop martop-50">
                    <div class="sec-title">
                        <h1 class="white-color">Services</h1>
                    </div>
                    <ul class="usefull-links">
                        <li><a href="javascript:void(0)"><i class="fa fa-angle-right" aria-hidden="true"></i>Expertise Comptable</a></li>
                        <li><a href="javascript:void(0)"><i class="fa fa-angle-right" aria-hidden="true"></i>Audit et Certification</a></li>
                        <li><a href="javascript:void(0)"><i class="fa fa-angle-right" aria-hidden="true"></i>Fiscalité</a></li>
                        <li><a href="javascript:void(0)"><i class="fa fa-angle-right" aria-hidden="true"></i>Conseil</a></li>
                        <li><a href="javascript:void(0)"><i class="fa fa-angle-right" aria-hidden="true"></i>Juridique</a></li>
                    </ul>
                </div>
            </div>
            <!--// [BEGIN] Bloc //-->

            <!--// [BEGIN] Bloc //-->
            <div class="col-xl-2 col-lg-4 col-md-6 col-sm-12">
                <div class="single-footer-widget martop martop-50">
                    <div class="sec-title">
                        <h1 class="white-color">Secteurs</h1>
                    </div>
                    <ul class="usefull-links">
                        <li><a href="javascript:void(0)"><i class="fa fa-angle-right" aria-hidden="true"></i>Associatif</a></li>
                        <li><a href="javascript:void(0)"><i class="fa fa-angle-right" aria-hidden="true"></i>Coopératif</a></li>
                        <li><a href="javascript:void(0)"><i class="fa fa-angle-right" aria-hidden="true"></i>Agroalimentaire</a></li>
                        <li><a href="javascript:void(0)"><i class="fa fa-angle-right" aria-hidden="true"></i>Assurance</a></li>
                        <li><a href="javascript:void(0)"><i class="fa fa-angle-right" aria-hidden="true"></i>Télécommunications</a></li>
                    </ul>
                </div>
            </div>
            <!--// [BEGIN] Bloc //-->

            <!--// [BEGIN] Bloc //-->
            <div class="col-xl-2 col-lg-4 col-md-6 col-sm-12">
                <div class="single-footer-widget martop martop-50">
                    <div class="sec-title">
                        <h1 class="white-color"> &nbsp; </h1>
                    </div>
                    <ul class="usefull-links">
                        <li><a href="javascript:void(0)"><i class="fa fa-angle-right" aria-hidden="true"></i>Associatif</a></li>
                        <li><a href="javascript:void(0)"><i class="fa fa-angle-right" aria-hidden="true"></i>Coopératif</a></li>
                        <li><a href="javascript:void(0)"><i class="fa fa-angle-right" aria-hidden="true"></i>Agroalimentaire</a></li>
                        <li><a href="javascript:void(0)"><i class="fa fa-angle-right" aria-hidden="true"></i>Assurance</a></li>
                        <li><a href="javascript:void(0)"><i class="fa fa-angle-right" aria-hidden="true"></i>Télécommunications</a></li>
                    </ul>
                </div>
            </div>
            <!--// [BEGIN] Bloc //-->


        </div>
    </div>
</footer>
<!--// [END] Footer //-->

<!--// [BEGIN] Bottom Area //-->
<section class="footer-bottom-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="footer-bottom">
                    <div class="col-xl-6 copyright-text pull-left">
                        <p>© Copyright 2018 FIZAZI & ASSOCIÉS. Tous droits réservés</p>
                    </div>

                    <div class="col-xl-2 copyright-text pull-right">
                        <div class="social-links-box">
                            <ul class="sociallinks-style-one">
                                <li><a href="javascript:void(0)"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="javascript:void(0)"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="javascript:void(0)"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                <li><a href="javascript:void(0)"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>



                </div>
            </div>
        </div>
    </div>
</section>
<!--// [END] Bottom Area //-->