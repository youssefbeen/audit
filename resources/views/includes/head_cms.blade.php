


    <!--// [BEGIN] Header //-->
    <header class="header-area">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12">
                    <div class="inner-content clearfix">

                        <div class="header-left float-left">
                            <div class="logo">
                                <a href="javascript:void(0)">
                                    <img src="{{asset('cms_asset/assets/images/resources/logo.png')}}">
                                </a>
                            </div>
                        </div>
                        <div class="header-right float-right">
                            <ul class="header-contact-info float-left">
                                <li class="single-info">
                                    <div class="icon-holder">
                                        <span class="icon-technology"></span>
                                    </div>
                                    <div class="text-holder">
                                        <span class="tel"></span>
                                        {{--<h4 class="email">contact@fizazi.ma</h4>--}}
                                    </div>
                                </li>
                                <li class="single-info">
                                    <div class="icon-holder">
                                        <span class="icon-signs"></span>
                                    </div>
                                    <div class="text-holder">
                                        <span class="adresse"></span>
                                    </div>
                                </li>
                                <li class="single-info">
                                    <button type="button" class="btn btn-primary" data-toggle="modal"
                                            data-target="#editInfos">
                                        Edit
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!--// [END] Header //-->