<!-- Modal -->
<div class="modal fade" id="createSecteur" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h3 class="modal-title" id="lineModalLabel">My Modal</h3>
            </div>
            <div class="modal-body">

                <!-- content goes here -->
                <form action="{{route('store_secteur')}}" method="post" class="dropzone" id="create-secteur">
                    {{csrf_field()}}
                    <div class="form-group" id="name">
                        <label for="name">Titre </label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Titre">
                    </div>

                    <div class="form-group" id="description_div">
                        <label for="description">Description</label>
                        <textarea type="text" class="form-control" id="description" placeholder="Description"
                                  name="description"></textarea>
                    </div>

                    <div class="dz-message"></div>
                    <div class="upl-box">
                        Upload Media
                    </div>
                    <div class="dz-preview dz-file-preview"></div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-default submit sub-drop">Submit</button>
                    </div>
                </form>

            </div>

        </div>
    </div>
</div>


