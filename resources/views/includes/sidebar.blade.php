
<div class="o-page__sidebar js-page-sidebar">
    <aside class="c-sidebar">
        <div class="c-sidebar__brand">
            <a href="{{route('dashboard')}}"><img src="img/logo.svg" alt="Neat"></a>
        </div>

        <!-- Scrollable -->
        <div class="c-sidebar__body">
            <span class="c-sidebar__title">Dashboards</span>
            <ul class="c-sidebar__list">
                <li>
                    <a class="c-sidebar__link {{Route::currentRouteName()=='client' ? "is-active" : '' }}"
                            href="{{route
                    ('client')
                    }}">
                        <i class="c-sidebar__icon feather icon-home"></i>Clients
                    </a>
                </li>
                <li>
                    <a class="c-sidebar__link {{Route::currentRouteName()=='facture' ? "is-active" : '' }}"
                       href="{{route('facture')}}">
                        <i class="c-sidebar__icon feather icon-bar-chart-2"></i>Facturations
                    </a>
                </li>
                <li>
                    <a class="c-sidebar__link {{Route::currentRouteName()=='cms' ? "is-active" : '' }}"
                       href="dashboard03.html">
                        <i class="c-sidebar__icon feather icon-pie-chart"></i>CMS
                    </a>
                </li>
                <li>
                    <a class="c-sidebar__link {{Route::currentRouteName()=='users' ? "is-active" : '' }}" href="{{route
                    ('users')}}">
                        <i class="c-sidebar__icon feather icon-pie-chart"></i>Utilisateurs
                    </a>
                </li>
            </ul>

        </div>




        <a class="c-sidebar__footer" href="{{route('logout')}}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            Logout <i class="c-sidebar__footer-icon feather icon-power"></i>
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
    </aside>
</div>

