<!-- Modal -->
<div class="modal fade" id="createMembre" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h3 class="modal-title" id="lineModalLabel">My Modal</h3>
            </div>
            <div class="modal-body">

                <!-- content goes here -->
                <form action="{{route('store_membre')}}" method="post" class="dropzone" id="create-membre">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="name"> Name </label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Titre">
                    </div>
                    <div class="form-group">
                        <label for="position">Position </label>
                        <input type="text" class="form-control" id="position" name="position" placeholder="Titre">
                    </div>
                    <div class="form-group">
                        <label for="email">email </label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Titre">
                    </div>
                    <div class="form-group">
                        <label for="linkedin">linkedin </label>
                        <input type="text" class="form-control" id="linkedin" name="linkedin"
                               placeholder="Linkedin">
                    </div>
                    <div class="form-group">
                        <label for="facebook">facebook </label>
                        <input type="text" class="form-control" id="facebook" name="facebook"
                               placeholder="facebook">
                    </div>
                    <div class="form-group">
                        <label for="twitter">twitter </label>
                        <input type="text" class="form-control" id="twitter" name="twitter"
                               placeholder="twitter">
                    </div>

                    <div class="dz-message"></div>
                    <div class="upl-box">
                        Upload Media
                    </div>
                    <div class="dz-preview dz-file-preview"></div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-default submit sub-drop">Submit</button>
                    </div>
                </form>

            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="editMembre" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h3 class="modal-title" id="lineModalLabel">My Modal</h3>
            </div>
            <div class="modal-body">

                <!-- content goes here -->
                <form method="post" >
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="name"> Name </label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Titre">
                    </div>
                    <div class="form-group">
                        <label for="position">Position </label>
                        <input type="text" class="form-control" id="position" name="position" placeholder="Titre">
                    </div>
                    <div class="form-group">
                        <label for="email">email </label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Titre">
                    </div>
                    <div class="form-group">
                        <label for="linkedin">linkedin </label>
                        <input type="text" class="form-control" id="linkedin" name="linkedin"
                               placeholder="Linkedin">
                    </div>
                    <div class="form-group">
                        <label for="facebook">facebook </label>
                        <input type="text" class="form-control" id="facebook" name="facebook"
                               placeholder="facebook">
                    </div>
                    <div class="form-group">
                        <label for="twitter">twitter </label>
                        <input type="text" class="form-control" id="twitter" name="twitter"
                               placeholder="twitter">
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-default submit sub-drop">Submit</button>
                    </div>
                </form>

            </div>

        </div>
    </div>
</div>


