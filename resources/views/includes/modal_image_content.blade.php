<!-- Modal -->
<div class="modal fade" id="editContent" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h3 class="modal-title" id="lineModalLabel">My Modal</h3>
            </div>
            <div class="modal-body">

                <!-- content goes here -->
                <form>
                    {{csrf_field()}}
                    <div class="form-group" id="titre1">
                        <label for="title1">Titre </label>
                        <input type="text" class="form-control" id="title1" name="title1" placeholder="Titre">
                    </div>
                    <div class="form-group" id="titre2">
                        <label for="title2">Sous titre</label>
                        <input type="text" class="form-control" id="title2" name="title2" placeholder="Sous Titre">
                    </div>
                    <div class="form-group" id="description_div">
                        <label for="description">Description</label>
                        <textarea type="text" class="form-control" id="description" placeholder="Description"
                                  name="description"></textarea>
                    </div>
                    <div class="form-group" id="boutton_div">
                        <label for="boutton">Boutton</label>
                        <input type="text" class="form-control" id="boutton" placeholder="boutton"
                               name="button">
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox"> Check me out
                        </label>
                    </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                </form>

            </div>

        </div>
    </div>
</div>