<section class="mainmenu-area stricky">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="innner-content clearfix">

                    <!--// [BEGIN] Main Menu //-->
                    <nav class="main-menu clearfix float-left">
                        <div class="navbar-header clearfix">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li class="current"><button type="button" class="btn btn-primary" data-toggle="modal"
                                                            data-target="#editNav" data-id="1">
                                        edit
                                    </button><a href="{{route('home')}}" id="nav_1">
                                    </a></li>
                                <li class="dropdown"><button type="button" class="btn btn-primary" data-toggle="modal"
                                                             data-target="#editNav" data-id="2">
                                        edit
                                    </button><a href={{route('cabinet')}} id="nav_2"></a></li>
                                <li class="dropdown"><button type="button" class="btn btn-primary" data-toggle="modal"
                                                             data-target="#editNav" data-id="3">
                                        edit
                                    </button><a href="{{route('services')}}" id="nav_3">Services</a>
                                    <ul>
                                        @foreach($services as$s)
                                        <li><a href="{{route('show_service', $s->id)}}">{{$s->name}}</a></li>
                                            @endforeach
                                    </ul>
                                </li>
                                <li class="dropdown"><button type="button" class="btn btn-primary" data-toggle="modal"
                                                             data-target="#editNav" data-id="4">
                                        edit
                                    </button><a href="{{route('secteurs')}}" id="nav_4"></a></li>
                                <li class="dropdown"><button type="button" class="btn btn-primary" data-toggle="modal"
                                                             data-target="#editNav" data-id="5">
                                        edit
                                    </button><a href="{{route('actualites')}}" id="nav_5"></a></li>
                                <li><button type="button" class="btn btn-primary" data-toggle="modal"
                                            data-target="#editNav" data-id="6">
                                        edit
                                    </button><a href="{{route('contact')}}" id="nav_6"></a></li>
                            </ul>
                        </div>
                    </nav>
                    <!--// [BEGIN] Main Menu //-->

                    <!--// [BEGIN] Menu Right //-->
                    <div class="mainmenu-right-box float-right">
                        <div class="button"><button type="button" class="btn btn-primary" data-toggle="modal"
                                                    data-target="#editNav" data-id="7">
                                edit
                            </button>
                            <a href="#" id="nav_7"></a>
                        </div>
                    </div>
                    <!--// [END] Menu Right //-->

                </div>
            </div>
        </div>
    </div>
</section>
<!--// [END] Menu //-->