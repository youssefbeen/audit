<!-- Modal -->
<div class="modal fade" id="editValue" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h3 class="modal-title" id="lineModalLabel">My Modal</h3>
            </div>
            <div class="modal-body">

                <!-- content goes here -->
                <form>
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="name">Titre </label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea type="text" class="form-control" id="description" placeholder="Description"
                                  name="description"></textarea>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="visible" value="1" id="visible"> Visible
                        </label>
                    </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                </form>

            </div>

        </div>
    </div>
</div>

