<!--// [BEGIN] News //-->

<?php
    $actualites = App\Actualite::orderBy('created_at','DESC')->get();
?>
<section class="latest-blog-area sec-pding-two">
    <div class="container">
        <div class="sec-title text-center">
            <p>Lorem ipsum dolor set amet</p>
            <h1>Dernières nouvelles</h1>
            <div class="border center"></div>
        </div>
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                <div class="blog-Carousel">
        @foreach($actualites as $a)
                    <!--// [BEGIN] Post //-->
                    <div class="single-blog-post">
                        <div class="row">
                            <div class="col-xl-6 col-lg-12 col-md-6 col-sm-12">
                                <div class="img-holder">
                                    <img src="{{asset('cms_asset/assets/media/'.$a->image)}}" alt="">
                                    <div class="overlay-style-one">
                                        <div class="box">
                                            <div class="content">
                                                <a href="javascript:void(0)"><i class="fa fa-link" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-12 col-md-6 col-sm-12">
                                <div class="text-holder">
                                    <div class="top clearfix">
                                        <div class="meta-box">
                                            <ul class="meta-info">
                                                <li><a href="javascript:void(0)">{{$a->user['name']}}</a></li>
                                                <li><a href="javascript:void(0)">{{$a->created_at->diffForHumans()
                                                }}</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="text">
                                        <h3><a class="blog-title" href="javascript:void(0)">{{$a->titre}}</a></h3>
                                        <p>{{str_limit($a->description, $limit = 100, $end = '...')}}</p>
                                        <div class="readmore">
                                            <a href="javascript:void(0)">En Savoir plus</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--// [END] Post //-->
            @endforeach




                </div>
            </div>

        </div>
    </div>
</section>
<!--// [END] News //-->