<!-- Modal -->
<div class="modal fade" id="editNav" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h3 class="modal-title" id="lineModalLabel">My Modal</h3>
            </div>
            <div class="modal-body">

                <!-- content goes here -->
                <form>
                    {{csrf_field()}}
                    <div class="form-group">
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                    <button type="submit" class="btn btn-default submit">Submit</button>
                </form>

            </div>

        </div>
    </div>
</div>


