<!-- Modal -->
<div class="modal fade" id="editInfos" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h3 class="modal-title" id="lineModalLabel">My Modal</h3>
            </div>
            <div class="modal-body">

                <!-- content goes here -->
                <form>
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="tel"> Tel </label>
                        <input type="text" class="form-control" id="tel" name="tel" placeholder="Tel">
                    </div>
                    <div class="form-group">
                        <label for="fax">Fax</label>
                        <input type="text" class="form-control" id="fax" name="fax" placeholder="Fax">
                    </div>
                    <div class="form-group">
                        <label for="Adresse">Adresse</label>
                        <textarea type="text" class="form-control" id="adresse" placeholder="Adresse"
                                  name="adresse"></textarea>
                    </div>

                    <button type="submit" class="btn btn-default">Submit</button>
                </form>

            </div>

        </div>
    </div>
</div>



{{--<!-- Modal -->--}}
{{--<div class="modal fade" id="editImage" tabindex="-1" role="dialog" aria-labelledby="modalLabel"--}}
     {{--aria-hidden="true">--}}
    {{--<div class="modal-dialog">--}}
        {{--<div class="modal-content">--}}
            {{--<div class="modal-header">--}}
                {{--<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>--}}
                {{--<h3 class="modal-title" id="lineModalLabel">My Modal</h3>--}}
            {{--</div>--}}
            {{--<div class="modal-body">--}}


                {{--<form action="/file-upload"--}}
                      {{--class="dropzone"--}}
                      {{--id="my-dropzone">--}}
                    {{--{{csrf_field()}}--}}
                    {{--<input type="hidden" class="id" name="id">--}}
                {{--</form>--}}


            {{--</div>--}}

        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}