@extends('layouts.admin_sidebar')

@section('content')




    <div class="container" style="margin-bottom: 35px">
        <a href="#" class="c-btn c-btn--info pull-right" data-toggle="modal" data-target="#add_facture"><span
                    class="glyphicon glyphicon-plus-sign"></span>  Ajouter Facture</a>
    </div>

        <!-- ======================= TABLES =======================-->


        <div class="row">
            <div class="col-12">
                <div class="c-table-responsive@wide">
                    <table class="c-table">
                        <thead class="c-table__head">
                        <tr class="c-table__row">
                            <th class="c-table__cell c-table__cell--head">N Facture</th>
                            <th class="c-table__cell c-table__cell--head">Client</th>
                            <!--<th class="c-table__cell c-table__cell&#45;&#45;head">Lead Score</th>-->
                            <th class="c-table__cell c-table__cell--head">Email</th>
                            <th class="c-table__cell c-table__cell--head">Montant</th>
                            <th class="c-table__cell c-table__cell--head">Status</th>
                            <!--<th class="c-table__cell c-table__cell&#45;&#45;head">Tags</th>-->
                            <th class="c-table__cell c-table__cell--head">Actions</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($factures as $f)
                        <tr class="c-table__row">
                            <td class="c-table__cell">{{$f->n_facture}}</td>
                            <td class="c-table__cell">
                                <div class="o-media">
                                    <div class="o-media__img u-mr-xsmall">
                                        <div class="c-avatar c-avatar--small">
                                            <img class="c-avatar__img" src={{asset('admin_asset/images/'.
                                            $f->client['image'])}} alt="Jessica Alba">
                                        </div>
                                    </div>
                                    <div class="o-media__body">
                                        <h6>{{$f->client['name']}}</h6>
                                        <!--<p>Graphic Designer</p>-->
                                    </div>
                                </div>
                            </td>

                            <!--<th class="c-table__cell">223</th>-->
                            <td class="c-table__cell">{{$f->client["email"]}}</td>
                            <td class="c-table__cell">{{$f->montant}} {{$f->currency['name']}}</td>
                            <td class="c-table__cell">
                                @if($f->status)
                                 <span class="c-badge c-badge--info c-tooltip
                                c-tooltip--top" aria-label='Transaction N 1234'><i class="fas fa-circle"></i>
                                     Paye</span>
                                @else
                                <i class="fas fa-circle-notch"></i> En cours
                                @endif
                            </td>
                            <!--<td class="c-table__cell">-->
                            <!--<a class="c-badge c-badge&#45;&#45;small c-badge&#45;&#45;info" href="#">sketch</a>-->
                            <!--<a class="c-badge c-badge&#45;&#45;small c-badge&#45;&#45;info" href="#">ui</a>-->
                            <!--<a class="c-badge c-badge&#45;&#45;small c-badge&#45;&#45;fancy" href="#">ux</a>-->
                            <!--</td>-->
                            <td class="c-table__cell">
                                <div class="c-dropdown dropdown">
                                    <a href="#" class="c-btn c-btn--info has-icon dropdown-toggle" id="dropdownMenuTable1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                                        More <i class="feather icon-chevron-down"></i>
                                    </a>

                                    <div class="c-dropdown__menu dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuTable1">
                                        @if($f->status)

                                        <a class="c-dropdown__item dropdown-item" data-toggle="modal"
                                           data-target="#joindre_facture"
                                           data-id="{{$f->n_facture}}" data-name = "{{$f->client['name']}}"
                                           data-email =
                                           "{{$f->client['email']}}" data-montant = "{{$f->montant}}" data-status
                                           ="{{$f->facture}}">
                                            {{($f->facture) ? 'Voir details' : 'Joindre facture'}}
                                        </a>
                                        @else
                                           <a class="c-dropdown__item dropdown-item c-btn c-btn--danger c-btn--outline"
                                              data-toggle="modal" data-target="#modal-delete-facture"
                                              data-id="{{$f->n_facture}}">Supprimer</a>
                                        @endif


                                    </div>
                                </div>
                            </td>
                        </tr>
                            @endforeach


                        </tbody>
                    </table>
                </div>
            </div>
        </div>


    <div class="c-modal c-modal--small modal fade" id="modal-delete-facture" tabindex="-1" role="dialog"
         aria-labelledby="modal-delete">
        <div class="c-modal__dialog modal-dialog" role="document">
            <div class="c-modal__content">
                <div class="c-modal__body">
                                    <span class="c-modal__close" data-dismiss="modal" aria-label="Close">
                                        <i class="feather icon-x"></i>
                                    </span>

                    <span class="c-icon c-icon--danger c-icon--large u-mb-small">
                                      <i class="feather icon-alert-triangle"></i>
                                    </span>
                    <h3 class="u-mb-small">Voulez-vous vraiment supprimer cette facture?</h3>

                    <div class="u-text-center">
                        <form id="delete-form" method="POST">
                            {{ csrf_field() }}
                            <a href="#" class="c-btn c-btn--danger c-btn--outline u-mr-small" data-dismiss="modal" aria-label="Close">Cancel</a>
                            <button class="c-btn c-btn--danger" id="btn-confirm" type="submit">Delete</button>
                        </form>
                    </div>
                </div>
            </div><!-- // .c-modal__content -->
        </div><!-- // .c-modal__dialog -->
    </div><!-- // .c-modal -->





        {{--<div class="row col-md-10 col-md-offset-1">--}}
            {{--<table class="table table-striped ">--}}

                {{--<tr>--}}
                    {{--<th>Facture</th>--}}
                    {{--<th>Client</th>--}}
                    {{--<th>Email</th>--}}
                    {{--<th>Montant</th>--}}
                    {{--<th>Status</th>--}}
                    {{--<th></th>--}}
                {{--</tr>--}}

                {{--<tbody class="border-tab">--}}
                {{--@foreach($factures as $f)--}}
                {{--<tr>--}}
                    {{--<td>{{$f->n_facture}}</td>--}}
                    {{--<td>{{$f->client['name']}}</td>--}}
                    {{--<td>{{$f->client['email']}}</td>--}}
                    {{--<td>{{$f->montant}} {{$f->currency['name']}}</td>--}}
                    {{--<td>--}}
                        {{--@if($f->status)--}}
                            {{--<i class="fas fa-circle"></i> Paye--}}
                        {{--@else--}}
                            {{--<i class="fas fa-circle-notch"></i> Encours--}}
                        {{--@endif--}}
                    {{--</td>--}}
                    {{--<td class="text-center">--}}
                        {{--<div class="dropdown">--}}
                            {{--<button class="btn  dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">--}}
                                {{--<span class="glyphicon glyphicon-option-vertical"></span>--}}
                            {{--</button>--}}
                            {{--<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">--}}
                                {{--<li><a href="#">Action</a></li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}

                    {{--</td>--}}
                {{--</tr>--}}
                {{--@endforeach--}}

                {{--</tbody>--}}
            {{--</table>--}}
        {{--</div>--}}

    <div class="c-modal modal fade" id="add_facture" tabindex="-1" role="dialog" aria-labelledby="add_facture">
        <div class="c-modal__dialog modal-dialog" role="document">
            <div class="c-modal__content">
                <div class="c-modal__body">
                <span class="c-modal__close" data-dismiss="modal" aria-label="Close">
                    <i class="feather icon-x"></i>
                </span>

                    <h3 class="u-mb-small" >Ajouter Facture</h3>
                    <form action="{{route('facture')}}" method="post" id="facture_form">                        {{ csrf_field() }}
                        {{csrf_field()}}
                        <div class="container" >
                            <div class="row"  style="margin-bottom: 20px">
                                <div class="col-md-6">
                                    <input type="text" class="c-input" id="n_facture" placeholder="N Facture"
                                           name="n_facture">
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="c-input" id="name" placeholder="Nom Client" name="name">

                                </div>
                            </div>
                            <div class="row"  style="margin-bottom: 20px">
                                <div class="col-md-6">
                                    <input type="email" id="email" name="email" class="c-input" placeholder="Email">
                                </div>
                                <div class="col-md-3" style="padding-right: 0;">
                                    <input type="text" id="montant" name="montant" class="c-input"
                                           placeholder="Montant">
                                </div>
                                <div class="col-md-3 c-select u-mb-xsmall"  style="padding-left: 0;">
                                    <select class="c-select__input" name="currency_id" id="currency_id">
                                            <option value="">Currency</option>
                                        @foreach($currencies as $c)
                                            <option value="{{$c->id}}">{{$c->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                            </div>
                            <div class="row"  style="margin-bottom: 40px">
                                <div class="col-md-12">
                                    <textarea name="note" id="note" cols="30" rows="5" class="c-input"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="o-line" style="margin-top: 20px">
                            <a href="#" class="c-btn c-btn--info c-btn--outline" data-dismiss="modal" aria-label="Close">Cancel</a>
                            <button class="c-btn c-btn--info" type="submit">Ajouter</button>
                        </div>
                    </form>
                </div>
            </div><!-- // .c-modal__content -->
        </div><!-- // .c-modal__dialog -->
    </div><!-- // .c-modal -->




    <div class="c-modal modal fade" id="joindre_facture" tabindex="-1" role="dialog" aria-labelledby="joindre_facture">
        <div class="c-modal__dialog modal-dialog" role="document">
            <div class="c-modal__content">
                <div class="c-modal__body">
                <span class="c-modal__close" data-dismiss="modal" aria-label="Close">
                    <i class="feather icon-x"></i>
                </span>

                    <h3 class="u-mb-small" >Joindre Facture</h3>
                    <div class="container ">
                        <div class="row u-mv-small ">
                             <p class="u-text-large">N Facture : </p> <span id="numero"> </span>
                        </div>
                        <div class="row u-mv-small">
                             <p class="u-text-large">Client : </p> <span id="client"></span><span id="mail"
                                                                                                  class="u-ml-xlarge"></span>
                        </div>
                        <div class="row u-mv-small">
                             <p class="u-text-large">Montant : </p> <span id="montant"></span>
                        </div>
                        <div class="row u-mv-large u-border-top u-border-bottom u-border-left u-border-right">
                             <div class="col-md-6">
                                 <img src="" alt="">
                             </div>
                            <div class="col-md-6
                            text-center">
                                <div class="row u-mv-small">
                                    <p class="u-text-large">Transaction N: 441</p>
                                </div>
                                <div class="row ">
                                    <p class="u-text-large">Date: 16/05/2018</p>
                                </div>
                             </div>
                        </div>



                        <div class="row u-mv-large ">
                            <div id="upl" class="col-md-offset-3">Upload your file</div>
                            <div id="pdf" class="col-md-offset-3">
                                <a id="link_pdf"><img src="{{ asset('images/pdf.png') }}" width="75%"></a>
                                <span><a id="change">Change</a></span>
                            </div>
                            {{--<form  id="joindre_facture_form"  class="dropzone" action="kxcn">--}}
                            {{--{{csrf_field()}}--}}
                            {{--</form>--}}
                            {{--<label class="col-md-6 col-md-offset-3" id="label"> Joindre Facture <input--}}
                                        {{--type="file" name="fact" id="fact"></label>--}}
                        </div>

                        <div class="o-line" style="margin-top: 20px">
                            <a href="#" class="c-btn c-btn--info c-btn--outline" data-dismiss="modal"
                               aria-label="Close">Cancel</a>
                            <button class="c-btn c-btn--info" data-dismiss="modal"
                                    aria-label="Close" onclick="location.reload();">Ajouter</button>
                        </div>

                </div>
            </div><!-- // .c-modal__content -->
        </div><!-- // .c-modal__dialog -->
    </div><!-- // .c-modal -->
    </div>
@stop