<!DOCTYPE html>
<html>
@include('includes.head')

<body>


<div class="o-page" id="app">

        @include('includes.sidebar')

        <main class="o-page__content">
         @include('includes.header')
            <div class="container">
                @if(session()->has('status'))
                    <div class="c-alert c-alert--{{ session('status') }} alert flash">
                        <span class="c-alert__icon">
                          <i class="feather icon-info"></i>
                        </span>
                     <div class="c-alert__content">
                            <p>{!! session('message') !!}</p>
                        </div>
                        <button class="c-close" data-dismiss="alert" type="button">×</button>
                    </div>
                    {{--<div class="alert alert-{{ session('status') }} flash">--}}
                     {{--{!! session('message') !!}--}}
                    {{--</div>--}}
                @endif
            @yield('content')

            </div>
    </main>
</div>

<script src="{{asset('admin_asset/js/neat.min.js')}}"></script>
{{--<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="--}}
        {{--crossorigin="anonymous"></script>--}}
<script src="{{ asset('js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.js"></script>
<script
        src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"
        integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk="
        crossorigin="anonymous"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js'></script>

<script src="{{asset('js/validation.js')}}"></script>


</body>