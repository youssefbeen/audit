<!DOCTYPE html>
<html>
@include('includes.head')

<body>


    <div class="o-page" id="app">
    <header class="c-navbar u-mb-large">
        <a class="c-navbar__brand" href="{{route('dashboard')}}">
            <img src="img/logo.svg" alt="Neat" title="Neat UI Kit">
        </a>

        <!-- Navigation items that will be collaped and toggled in small viewports -->
        <nav class="c-navbar__nav collapse" id="main-nav">
            <ul class="c-navbar__nav-list">
                <li class="c-navbar__nav-item">
                    <a class="c-navbar__nav-link" href="index.html">Home</a>
                </li>
                <li class="c-navbar__nav-item">
                    <a class="c-navbar__nav-link" href="dashboard01.html">Dashboard</a>
                </li>
                <li class="c-navbar__nav-item">
                    <a class="c-navbar__nav-link" href="pricing.html">Pricing</a>
                </li>

                <li class="c-navbar__nav-item">
                    <a class="c-navbar__nav-link" href="faq.html">Faq</a>
                </li>
            </ul>
        </nav>
        <!-- // Navigation items  -->

        <div class="c-dropdown dropdown u-mr-small u-ml-auto">
            {{--<div class="c-notification dropdown-toggle" id="dropdownMenuToggle1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">--}}
                {{--<i class="c-notification__icon feather icon-message-circle"></i>--}}
            {{--</div>--}}

            {{--<div class="c-dropdown__menu c-dropdown__menu--large has-arrow dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuToggle1">--}}

              {{--<span class="c-dropdown__menu-header">--}}
                {{--Mentions--}}
              {{--</span>--}}
                {{--<a class="c-dropdown__item dropdown-item" href="#">--}}
                    {{--<div class="o-media">--}}
                        {{--<div class="o-media__img u-mr-xsmall">--}}
                    {{--<span class="c-avatar c-avatar--xsmall">--}}
                      {{--<img class="c-avatar__img" src="http://via.placeholder.com/72" alt="Adam Sandler">--}}
                    {{--</span>--}}
                        {{--</div>--}}

                        {{--<div class="o-media__body">--}}
                            {{--<p>Hey, Julia how are you doing. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat eius iste.</p>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</a>--}}

                {{--<a class="c-dropdown__item dropdown-item" href="#">--}}
                    {{--<div class="o-media">--}}
                        {{--<div class="o-media__img u-mr-xsmall">--}}
                    {{--<span class="c-avatar c-avatar--xsmall">--}}
                      {{--<img class="c-avatar__img" src="http://via.placeholder.com/72" alt="Adam Sandler">--}}
                    {{--</span>--}}
                        {{--</div>--}}

                        {{--<div class="o-media__body">--}}
                            {{--<p>Hey, Julia how are you doing. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat eius iste.</p>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</a>--}}

                {{--<a class="c-dropdown__item dropdown-item" href="#">--}}
                    {{--<div class="o-media">--}}
                        {{--<div class="o-media__img u-mr-xsmall">--}}
                    {{--<span class="c-avatar c-avatar--xsmall">--}}
                      {{--<img class="c-avatar__img" src="http://via.placeholder.com/72" alt="Adam Sandler">--}}
                    {{--</span>--}}
                        {{--</div>--}}

                        {{--<div class="o-media__body">--}}
                            {{--<p>Hey, Julia how are you doing. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat eius iste.</p>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</a>--}}

                {{--<a class="c-dropdown__menu-footer">--}}
                    {{--All Mentions--}}
                {{--</a>--}}
            {{--</div>--}}
        {{--</div>--}}

        {{--<div class="c-dropdown dropdown u-mr-medium">--}}
            {{--<div class="c-notification has-indicator dropdown-toggle" id="dropdownMenuToggle2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">--}}
                {{--<i class="c-notification__icon feather icon-bell"></i>--}}
            {{--</div>--}}

            {{--<div class="c-dropdown__menu c-dropdown__menu--large has-arrow dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuToggle2">--}}

              {{--<span class="c-dropdown__menu-header">--}}
                {{--Notifications--}}
              {{--</span>--}}
                {{--<a class="c-dropdown__item dropdown-item" href="#">--}}
                    {{--<div class="o-media">--}}
                        {{--<div class="o-media__img u-mr-xsmall">--}}
                            {{--<span class="c-icon c-icon--info c-icon--xsmall"><i class="feather icon-globe"></i></span>--}}
                        {{--</div>--}}

                        {{--<div class="o-media__body">--}}
                            {{--<p>We've updated the Stripe Services agreement and its supporting terms. Your continueduse of Stripe's services.</p>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</a>--}}

                {{--<a class="c-dropdown__item dropdown-item" href="#">--}}
                    {{--<div class="o-media">--}}
                        {{--<div class="o-media__img u-mr-xsmall">--}}
                            {{--<span class="c-icon c-icon--danger c-icon--xsmall"><i class="feather icon-x"></i></span>--}}
                        {{--</div>--}}

                        {{--<div class="o-media__body">--}}
                            {{--<p>We've updated the Stripe Services agreement and its supporting terms. Your continueduse of Stripe's services.</p>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</a>--}}

                {{--<a class="c-dropdown__item dropdown-item" href="#">--}}
                    {{--<div class="o-media">--}}
                        {{--<div class="o-media__img u-mr-xsmall">--}}
                            {{--<span class="c-icon c-icon--success c-icon--xsmall"><i class="feather icon-anchor"></i></span>--}}
                        {{--</div>--}}

                        {{--<div class="o-media__body">--}}
                            {{--<p>We've updated the Stripe Services agreement and its supporting terms. Your continueduse of Stripe's services.</p>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</a>--}}

                {{--<a class="c-dropdown__menu-footer">--}}
                    {{--All Notifications--}}
                {{--</a>--}}
            {{--</div>--}}
        </div>

        <div class="c-dropdown dropdown">
            <div class="c-avatar c-avatar--xsmall dropdown-toggle" id="dropdownMenuAvatar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                <img class="c-avatar__img " src="{{ asset('admin_asset/images/'. Auth::user()->image )}}"> <h1 class="u-h6
            u-text-uppercase u-opacity-medium u-pl-small"> {{Auth::user()->name}}</h1>
            </div>


            <div class="c-dropdown__menu has-arrow dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuAvatar">
                <a class="c-dropdown__item dropdown-item" href="{{route('profile_user', Auth::user()->id)}}">View
                    Profile</a>
                <a class="c-dropdown__item dropdown-item" href="{{route('edit_user', Auth::user()->id)}}">Edit Profile</a>
                {{--<a class="c-dropdown__item dropdown-item" href="#">View Activity</a>--}}
                <a class="c-dropdown__item dropdown-item" href="{{route('logout')}}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Log out</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
        <button class="c-navbar__nav-toggle" type="button" data-toggle="collapse" data-target="#main-nav">
            <i class="feather icon-menu"></i>
        </button><!-- // .c-nav-toggle -->
    </header>
<div class="container">

    @yield('content')


</div>


</div>
<script src="{{asset('admin_asset/js/neat.min.js')}}"></script>
    <script src="{{ asset('js/app.js') }}"></script>

</body>